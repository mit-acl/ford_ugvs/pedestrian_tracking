# ped_tracking

## About

This repo holds the pedestrian tracking software used in ACL jackal experiments in the 2016-2019 timeframe, including the video of the jackal driving in Stata.

The basic idea is to subscribe to a laserscan msg, convert that to a pointcloud (because that makes it easier to manipulate with PCL), and run that pointcloud through the DynamicMeans clustering algorithm.

Fancier versions of the code fuse data from the laserscanner and cameras to assign a likelihood of each cluster being a pedestrian, track the pedestrian trajectories over time, predict future motion of pedestrians using GPs, remove pedestrians from a costmap, etc. but these things are not set to run by default.

### Repository Structure

The main ROS pkg is `ped_tracking`, which currently only holds a launch file.
The `pointcloud` directory has a ROS-ified implementation of the Dynamic Means clustering algorithm and also has a copy of a laserscan-to-pointcloud conversion script (that should probably get replaced by a standard ROS library).
The `ped_master` directory has code for tracking pedestrians over time, which was mainly developed for Justin Miller's projects that involved estimating pedestrian flows across campus with golf carts.
The `image` directory has code for putting bounding boxes around pedestrians in images (`cv_tracker` has an SSD node extended from Shayegan's UAV work, `human_detector` has some scripts to fuse bounding boxes and clusters, and the VeryFast detector node seems to have disappeared somewhere along the way).

## Install

```bash
# Install all ROS dependencies
rosdep update
rosdep install --from-paths src --ignore-src -r -y

# compile the packages
catkin build
```

## Run

```bash
roslaunch ped_tracking ped_tracking.launch scan_topic:=/jackal1/
```

You should see msgs on `/cluster/output/clusters`, and you can visualize the clusters in RViz as a MarkerArray on `cluster/output/marker_array` -- they'll appear as rectangles with IDs around cluster of laserscan points.
The image below shows an example of the RViz view of a successful case: green points are the laserscan, the various colors rectangles are the clustering algorithm's estimate of the bounding rectangle with ID numbers (0, 1, 6).

![example of clusters](misc/clusters_example.png)


## Notes

By default, `ped_tracking.launch` only uses a laserscan to find and track "clusters" that could correspond to pedestrians.

If you want to fuse those clusters with camera data, the code for running the VeryFast or SSD detectors (these return bounding boxes around pedestrians in camera images) lives in this repo but it might be painful to get it to compile. Then, you would set `classify_pedestrians:=true` in `ped_tracking.launch`. By default, the image-related packages (in `image`) are ignored by catkin via a `CATKIN_IGNORE` file.