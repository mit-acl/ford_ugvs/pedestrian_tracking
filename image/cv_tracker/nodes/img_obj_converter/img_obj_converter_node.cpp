#include <string>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
//#include <runtime_manager/ConfigRcnn.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <acl_msgs/ImageObj.h>
#include <ford_msgs/potential_detections.h>

//#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <image_transport/image_transport.h>




class ImgObjToRect{
	ros::Publisher found_pub_front_left;
	ros::Publisher found_pub_front_right;
	ros::Publisher found_pub_front_middle;
	ros::Publisher found_pub_back_middle;

	ros::Subscriber found_sub_front_left;
	ros::Subscriber found_sub_front_right;
	ros::Subscriber found_sub_front_middle;
	ros::Subscriber found_sub_back_middle;


	ros::NodeHandle node_handle_;



	//vector of indices of the classes to search for
	std::vector<unsigned int> detect_classes_;



public:
    ImgObjToRect(){

        //ros::NodeHandle private_node_handle("~");//to receive args

        found_pub_front_left = node_handle_.advertise<ford_msgs::potential_detections>("/found_detections_front_left", 1);
        found_pub_front_right = node_handle_.advertise<ford_msgs::potential_detections>("/found_detections_front_right", 1);
        found_pub_front_middle = node_handle_.advertise<ford_msgs::potential_detections>("/found_detections_front_middle", 1);
        found_pub_back_middle = node_handle_.advertise<ford_msgs::potential_detections>("/found_detections_back_middle", 1);

        found_sub_front_left = node_handle_.subscribe("/person_img_obj_front_left", 1, &ImgObjToRect::image_callback_front_right,this);
        found_sub_front_right = node_handle_.subscribe("/person_img_obj_front_right", 1, &ImgObjToRect::image_callback_front_left,this);
        found_sub_front_middle = node_handle_.subscribe("/person_img_obj_front_middle", 1, &ImgObjToRect::image_callback_front_middle,this);
        found_sub_back_middle = node_handle_.subscribe("/person_img_obj_back_middle", 1, &ImgObjToRect::image_callback_back_middle,this );

    }

    void image_callback_front_right(const acl_msgs::ImageObj &img_obj){

        ford_msgs::potential_detections potential_detection;

        for(int i=0;i<img_obj.obj.size();i++){
            potential_detection.scores.push_back(img_obj.obj[i].score);
            potential_detection.detections.push_back(img_obj.obj[i].x);
            potential_detection.detections.push_back(img_obj.obj[i].y);
            potential_detection.detections.push_back(img_obj.obj[i].width);
            potential_detection.detections.push_back(img_obj.obj[i].height);
        }

        potential_detection.header.stamp = img_obj.header.stamp;


        found_pub_front_right.publish(potential_detection);

    }

    void image_callback_front_left(const acl_msgs::ImageObj &img_obj){



        ford_msgs::potential_detections potential_detection;

        for(int i=0;i<img_obj.obj.size();i++){
            potential_detection.scores.push_back(img_obj.obj[i].score);
            potential_detection.detections.push_back(img_obj.obj[i].x);
            potential_detection.detections.push_back(img_obj.obj[i].y);
            potential_detection.detections.push_back(img_obj.obj[i].width);
            potential_detection.detections.push_back(img_obj.obj[i].height);
        }

        potential_detection.header.stamp = img_obj.header.stamp;



        found_pub_front_left.publish(potential_detection);

    }
    void image_callback_front_middle(const acl_msgs::ImageObj &img_obj){

        ford_msgs::potential_detections potential_detection;

        for(int i=0;i<img_obj.obj.size();i++){
            potential_detection.scores.push_back(img_obj.obj[i].score);
            potential_detection.detections.push_back(img_obj.obj[i].x);
            potential_detection.detections.push_back(img_obj.obj[i].y);
            potential_detection.detections.push_back(img_obj.obj[i].width);
            potential_detection.detections.push_back(img_obj.obj[i].height);
        }

        potential_detection.header.stamp = img_obj.header.stamp;


        found_pub_front_middle.publish(potential_detection);

    }

    void image_callback_back_middle(const acl_msgs::ImageObj &img_obj){

        ford_msgs::potential_detections potential_detection;

        for(int i=0;i<img_obj.obj.size();i++){
            potential_detection.scores.push_back(img_obj.obj[i].score);
            potential_detection.detections.push_back(img_obj.obj[i].x);
            potential_detection.detections.push_back(img_obj.obj[i].y);
            potential_detection.detections.push_back(img_obj.obj[i].width);
            potential_detection.detections.push_back(img_obj.obj[i].height);
        }

        potential_detection.header.stamp = img_obj.header.stamp;


        found_pub_back_middle.publish(potential_detection);

    }
};

int main(int argc, char **argv){
	ros::init(argc, argv, "img_obj_converter");

	ImgObjToRect converter;

    ros::spin();

	return 0;
}
