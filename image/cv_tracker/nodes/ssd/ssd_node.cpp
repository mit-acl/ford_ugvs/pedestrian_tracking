/*
 *  Copyright (c) 2015, Nagoya University
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name of Autoware nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <string>
#include <ros/package.h>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
//#include <runtime_manager/ConfigRcnn.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <ford_msgs/ImageObj.h>
#include <ford_msgs/potential_detections.h>

#include <rect_class_score.h>

//#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "ssd_detector.h"
#include <image_transport/image_transport.h>

#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>

#include <boost/bind.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include <omp.h>

#include <stdio.h>

#include <math.h>

typedef struct
{
    ros::Publisher found_pub;
    image_transport::Publisher image_pub;
    ros::Publisher bb_pub;
    std::string camera_name;
} publishers;

class RosSsdApp{
    image_transport::Subscriber subscriber_image_raw_;
	ros::Subscriber subscriber_ssd_config_;
	ros::Publisher publisher_car_objects_;
	ros::Publisher publisher_person_objects_;
	ros::Publisher publisher_all_objects_;
	ros::NodeHandle node_handle_;
    image_transport::ImageTransport it_;

    std::map<std::string,image_transport::Subscriber> image_subs;
    std::map<std::string,image_transport::Publisher> image_pubs;
    std::map<std::string,image_transport::Publisher> blurred_image_pubs;
    std::map<std::string,image_transport::Publisher> cropped_image_pubs;
    std::map<std::string,ros::Publisher> found_pubs;
    std::map<std::string,ros::Publisher> bb_pubs;
    std::map<std::string,sensor_msgs::ImageConstPtr> latest_images;
    ros::Timer timer;

    std::map<std::string,bool> should_blur_image;
    std::map<std::string,bool> should_crop_image;
    std::map<std::string,std::vector<int>> cropped_image_dims;

	cv::Scalar pixel_mean_;

	//Caffe based Object Detection ConvNet
	SsdDetector* ssd_detector_;

	//The minimum score required to filter the detected objects by the ConvNet
	float score_threshold_;

	//If GPU is enabled, stores the GPU Device to use
	unsigned int gpu_device_id_;

	//Sets whether or not use GPU acceleration
	bool use_gpu_;

	//vector of indices of the classes to search for
	std::vector<unsigned int> detect_classes_;

	void convert_rect_to_image_obj(std::vector< RectClassScore<float> >& in_objects, ford_msgs::ImageObj& out_message, cv::Mat& in_image, std::string in_class){
		for (unsigned int i = 0; i < in_objects.size(); ++i){
			//check if the score is larger than minimum required
			if ( (in_objects[i].score > score_threshold_)
				&& (	(in_class == "all") 
					|| (in_class == "car" && (in_objects[i].class_type == Ssd::CAR || in_objects[i].class_type == Ssd::BUS))
					|| (in_class == "person" && (in_objects[i].class_type == Ssd::PERSON))
				)){
				ford_msgs::ImageRect rect;

				rect.x = in_objects[i].x;
				rect.y = in_objects[i].y;
				rect.width = in_objects[i].w;
				rect.height = in_objects[i].h;
				rect.class_string = in_objects[i].GetClassString();
				rect.class_id = in_objects[i].class_type;

				if (in_objects[i].x < 0)
					rect.x = 0;
				if (in_objects[i].y < 0)
					rect.y = 0;
				if (in_objects[i].w < 0)
					rect.width = 0;
				if (in_objects[i].h < 0)
					rect.height = 0;

				rect.score = in_objects[i].score;

				out_message.obj.push_back(rect);
			}
		}
        out_message.header.stamp = ros::Time::now();
	}
public:
	RosSsdApp(): it_(node_handle_){
		ssd_detector_ 	= NULL;
		score_threshold_= 0.5;
		use_gpu_ 		= false;
		gpu_device_id_ 	= 0;
		pixel_mean_		= cv::Scalar(102.9801, 115.9465, 122.7717);

        //ROS STUFF
        ros::NodeHandle private_node_handle("~");//to receive args

        //RECEIVE CONVNET FILENAMES
        std::string network_definition_file;
        std::string pretrained_model_file;
        if (private_node_handle.getParam("network_definition_file", network_definition_file)){
            ROS_INFO("Network Definition File: %s", network_definition_file.c_str());
        }
        else{
            ROS_INFO("No Network Definition File was received. Finishing execution.");
            return;
        }

        if (private_node_handle.getParam("pretrained_model_file", pretrained_model_file)){
            ROS_INFO("Pretrained Model File: %s", pretrained_model_file.c_str());
        }
        else{
            ROS_INFO("No Pretrained Model File was received. Finishing execution.");
            return;
        }

        if (private_node_handle.getParam("score_threshold", score_threshold_)){
            ROS_INFO("Score Threshold: %f", score_threshold_);
        }

        if (private_node_handle.getParam("use_gpu", use_gpu_)){
            ROS_INFO("GPU Mode: %d", use_gpu_);
        }

        int gpu_id;
        if (private_node_handle.getParam("gpu_device_id", gpu_id )){
            ROS_INFO("GPU Device ID: %d", gpu_id);
            gpu_device_id_ = (unsigned int) gpu_id;
        }

        //SSD STUFF
        ssd_detector_ = new SsdDetector(network_definition_file, pretrained_model_file, pixel_mean_, use_gpu_, gpu_device_id_);

        if (NULL == ssd_detector_){
            ROS_INFO("Error while creating SSD Object");
            return;
        }
        ROS_INFO("SSD Detector initialized.");

        publisher_car_objects_ = node_handle_.advertise<ford_msgs::ImageObj>("obj_car/image_obj", 1);
        publisher_person_objects_ = node_handle_.advertise<ford_msgs::ImageObj>("obj_person/image_obj", 1);
        publisher_all_objects_ = node_handle_.advertise<ford_msgs::ImageObj>("obj_all/image_obj", 1);


        /*
        Go through every camera (defined as ROS params in a .yaml file),
        and set up publishers/subscribers. Also check camera attributes, such
        as blur and crop, to determine whether to re-publish images with these
        modifications to enable anonymous data collection in the field.
        */
        std::string ns = ros::this_node::getNamespace();
        ROS_INFO_STREAM("SSD:" << ns);
        std::string camera_type, image_topic;
        XmlRpc::XmlRpcValue cameras;
        private_node_handle.getParam(ns+"/object_classifier/camera_type",camera_type);
        private_node_handle.getParam(ns+"/object_classifier/image_topic",image_topic);
        private_node_handle.getParam(ns+"/object_classifier/cameras", cameras);
        ROS_INFO_STREAM("SSD:" << camera_type);
        // ROS_ASSERT(cameras.getType() == XmlRpc::XmlRpcValue::TypeStruct);
        // int num_cameras = cameras.size();

        
        // for (int32_t i = 0; i < num_cameras; ++i) 
        int num_cameras = 0;
        for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator cameras_it = cameras.begin(); cameras_it != cameras.end(); ++cameras_it)
        {
            num_cameras++;
            std::string camera_location = cameras_it->first;
            std::string image_input_topic = camera_type+"_"+camera_location+"/"+image_topic;
            
            // Every camera gets a subscriber to raw image, publisher of bounding box rectangle,
            // image with bounding boxes, and found_pubs (i forget what it does)
            found_pubs[camera_location] = node_handle_.advertise<ford_msgs::ImageObj>("person_img_obj_"+camera_location, 1);
            image_pubs[camera_location] = it_.advertise("human_detection_image_"+camera_location, 1);
            bb_pubs[camera_location] = node_handle_.advertise<ford_msgs::potential_detections>("found_detections_"+camera_location, 1);
            image_subs[camera_location] = it_.subscribe(image_input_topic, 1, boost::bind(&RosSsdApp::image_callback, this, _1, camera_location));

            // Go through each camera and check if blur/crop should be published
            XmlRpc::XmlRpcValue camera_data = cameras_it->second;
            for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator camera_data_it = camera_data.begin(); camera_data_it != camera_data.end(); ++camera_data_it)
            {
                if (camera_data_it->first == "blur"){
                    XmlRpc::XmlRpcValue blur_value = camera_data_it->second;
                    bool blur = static_cast<bool>(blur_value);
                    if (blur){ // This camera should get re-published as blurred
                        should_blur_image[camera_location] = true;
                        blurred_image_pubs[camera_location] = it_.advertise("blurred_image_"+camera_location, 1);
                    }
                    else{
                        should_blur_image[camera_location] = false;
                    }
                }
                else if (camera_data_it->first == "crop"){
                    XmlRpc::XmlRpcValue crop_struct = camera_data_it->second;
                    XmlRpc::XmlRpcValue crop_toggle = crop_struct["toggle"];
                    bool crop = static_cast<bool>(crop_toggle);
                    if (crop){ // This camera should get re-published as cropped
                        XmlRpc::XmlRpcValue crop_dims_value = crop_struct["dimensions"];
                        for (int j = 0; j < 4; ++j){ // Add the 4 crop dimensions to an array to be used later
                            cropped_image_dims[camera_location].push_back(static_cast<int>(crop_dims_value[j]));
                        }
                        should_crop_image[camera_location] = true;
                        cropped_image_pubs[camera_location] = it_.advertise("cropped_image_"+camera_location, 1);
                    }
                    else{
                        should_crop_image[camera_location] = false;
                    }
                }
            }

        }

        float ssd_freq = 20.0;
        private_node_handle.getParam("ssd_freq",ssd_freq); // Hz for SSD to run
        float timer_freq = ssd_freq / num_cameras; // Hz per camera
        float timer_period = 1.0 / timer_freq; // sec/cycle per camera
        ROS_INFO_STREAM("SSD will operate at: " << ssd_freq << " Hz (" << timer_freq << " Hz per camera).");
        timer = private_node_handle.createTimer(ros::Duration(timer_period), boost::bind(&RosSsdApp::process_images, this, _1));

    }

    ~RosSsdApp(){
        if (NULL != ssd_detector_)
            delete ssd_detector_;
    }

    void process_images(const ros::TimerEvent& event){
        // ROS_INFO_STREAM("processing.");
        for(auto const &ent : latest_images) {
            // std::cout << "Processing " << ent.first << std::endl;
            double thresh; ros::param::getCached("~score_thresh", thresh);
            //Receive Image, convert it to OpenCV Mat
            cv_bridge::CvImagePtr cv_ptr;

            try
            {
                cv_ptr = cv_bridge::toCvCopy(ent.second, sensor_msgs::image_encodings::BGR8);
            }
            catch (cv_bridge::Exception& e)
            {
                ROS_ERROR("cv_bridge exception: %s", e.what());
                return;
            }

            cv::Mat image = cv_ptr->image;

            //Detect Object in image
            std::vector< RectClassScore<float> > detections;
            detections = ssd_detector_->Detect(image);

            //Prepare Output message
            ford_msgs::ImageObj output_car_message;
            ford_msgs::ImageObj output_person_message;
            ford_msgs::ImageObj output_allobjs_message;

            output_car_message.header = ent.second->header;
            output_car_message.type = "car";

            output_person_message.header = ent.second->header;
            output_person_message.type = "person";
            // ROS_INFO_STREAM("Camera name: " << pub.camera_name);
            // output_person_message.header.frame_id = pub.camera_name;

            output_allobjs_message.header = ent.second->header;
            output_allobjs_message.type = "all";


            //Convert Objects to Message type
            //convert_rect_to_image_obj(detections, output_car_message, image, "car");
            convert_rect_to_image_obj(detections, output_person_message, image, "person");
            // output_person_message.header.frame_id = pub.camera_name;
            //convert_rect_to_image_obj(detections, output_allobjs_message, image, "all");



            //draw bounding boxes on display image
            // ROS_INFO_STREAM("# Detections: " << detections.size());
            for (size_t i=0; i<detections.size(); i++){
                // std::cout << "Score: " << detections[i].score << ", Class: " << detections[i].class_type << std::endl;
                if(detections[i].score>=score_threshold_ && detections[i].class_type == Ssd::PERSON){
                // if((detections[i].score>=score_threshold_ && detections[i].class_type == Ssd::PERSON) &&
                //     (detections[i].score<=1.0 && detections[i].class_type == Ssd::CHAIR)){
                    cv::Scalar color(0,255,0);
                    // cv::Scalar color (0, 100 + ((int) round(1000 * (detections[i].score - thresh)) % 255), 0);
                    cv::rectangle(cv_ptr->image, cv::Point(detections[i].x,detections[i].y),cv::Point(detections[i].x + detections[i].w,detections[i].y + detections[i].h), color, 3);

                    //block out face
                    double face_height_ratio = 0.3;
                    int face_height = round(face_height_ratio * (detections[i].h));
                    int y_face = detections[i].y + face_height;
                    cv::rectangle(cv_ptr->image, cv::Point(detections[i].x,detections[i].y),cv::Point(detections[i].x + detections[i].w,y_face), color, CV_FILLED);
                }
            }

                    
            ford_msgs::potential_detections potential_detection;

            int num_objects = output_person_message.obj.size();
            for(int i=0;i<num_objects;i++){
                potential_detection.scores.push_back(output_person_message.obj[i].score);
                potential_detection.detections.push_back(output_person_message.obj[i].x);
                potential_detection.detections.push_back(output_person_message.obj[i].y);
                potential_detection.detections.push_back(output_person_message.obj[i].width);
                potential_detection.detections.push_back(output_person_message.obj[i].height);
            }

            potential_detection.header.stamp = output_person_message.header.stamp;
            potential_detection.header.frame_id = output_person_message.header.frame_id;

            found_pubs[ent.first].publish(output_person_message);
            bb_pubs[ent.first].publish(potential_detection);

            image_pubs[ent.first].publish(cv_ptr->toImageMsg());
        }
    }

    void image_callback(const sensor_msgs::ImageConstPtr& msg, std::string camera){
        // Just store the latest image, which will be processed
        // on a fixed timer interval by SSD.
        latest_images[camera] = msg;

        cv_bridge::CvImagePtr cv_ptr;
        if (should_blur_image[camera] || should_crop_image[camera]){
            try
            {
                cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            }
            catch (cv_bridge::Exception& e)
            {
                ROS_ERROR("cv_bridge exception: %s", e.what());
                return;
            }
        }

        if (should_blur_image[camera]){

            // Blur and publish image
            cv::Mat blurred_img = cv_ptr->image.clone();
            cv::blur( cv_ptr->image, blurred_img, cv::Size( 100, 100 ), cv::Point(-1,-1) );
            
            cv_bridge::CvImage blurred_img_bridge;
            sensor_msgs::Image blurred_img_msg; // >> message to be sent
            blurred_img_bridge = cv_bridge::CvImage(msg->header, sensor_msgs::image_encodings::RGB8, blurred_img);
            blurred_img_bridge.toImageMsg(blurred_img_msg); // from cv_bridge to sensor_msgs::Image
            blurred_image_pubs[camera].publish(blurred_img_msg);

        }
        if (should_crop_image[camera]){

            // Crop and publish image
            int x1 = cropped_image_dims[camera][0];
            int y1 = cropped_image_dims[camera][1];
            int x2 = cropped_image_dims[camera][2];
            int y2 = cropped_image_dims[camera][3];
            cv::Mat cropped_img(cv_ptr->image, cv::Rect(x1,y1,x2,y2));

            cv_bridge::CvImage cropped_img_bridge;
            sensor_msgs::Image cropped_img_msg; // >> message to be sent
            cropped_img_bridge = cv_bridge::CvImage(msg->header, sensor_msgs::image_encodings::RGB8, cropped_img);
            cropped_img_bridge.toImageMsg(cropped_img_msg); // from cv_bridge to sensor_msgs::Image
            cropped_image_pubs[camera].publish(cropped_img_msg);
        }

    }

    
};

int main(int argc, char **argv){
	ros::init(argc, argv, "ssd_unc");

	RosSsdApp app;

    ros::spin();

    ROS_INFO("END Ssd");

	return 0;
}
