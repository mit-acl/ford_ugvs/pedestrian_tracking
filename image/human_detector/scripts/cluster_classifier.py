#!/usr/bin/env python
import roslib
roslib.load_manifest('human_detector')
import sys
import math
import rospy
import tf
from ford_msgs.msg import Clusters
from ford_msgs.msg import ClusterHit
from ford_msgs.msg import ped_detection
from std_msgs.msg import UInt32
from geometry_msgs.msg import Point, PointStamped
from visualization_msgs.msg import MarkerArray, Marker
from visualization_msgs.msg import MarkerArray, Marker
from copy import deepcopy
from threading import Lock
from random import random
import numpy

class ClusterClassifier(object):

    def __init__(self):

        self.tf_listener = tf.TransformListener();

        self.unit_vectors=[]
        self.clusters=Clusters()
        self.cluster_hit=ClusterHit()
        self.ped_detection_dict = dict()

        self.timer_period = rospy.get_param("~timer_period",default=(1.0/10.0))
        self.max_cam_radius = rospy.get_param("~max_cam_radius",default=10)
        self.min_cam_radius = rospy.get_param("~min_cam_radius",default=1.65)
        self.cam_max_angle = rospy.get_param("~cam_max_angle",default=90) #Total viewing angle cone in degrees
        self.sigma = rospy.get_param("~sigma",default=0.1)
        self.reliable_detection_range = rospy.get_param("~reliable_detection_range",5.0)
        self.hit_likelihood_boost = rospy.get_param("~hit_likelihood_boost",1.0)

        self.in_view_penalty = rospy.get_param("~in_view_penalty", default=.1)

        self.arrow_pub = rospy.Publisher("unit_vectors",MarkerArray, queue_size=1)
        self.sphere_pub = rospy.Publisher("cluster_hits",MarkerArray, queue_size=10)
        self.pedestrian_cluster_id_pub = rospy.Publisher("pedestrian_cluster_id", ClusterHit, queue_size=10)
        self.likelihood_pub = rospy.Publisher("cluster_hit_likelihood",MarkerArray, queue_size=10)

        self.unit_vectors_sub = rospy.Subscriber("~ped_detections",ped_detection,self.ped_detection_callback)
        self.clusters_sub = rospy.Subscriber("~clusters", Clusters,self.clusters_callback)

        self.timer = rospy.Timer(rospy.Duration.from_sec(self.timer_period),self.process_callback)        
        
    def clusters_callback(self,clusters):
        self.clusters=clusters

    def ped_detection_callback(self,ped_detections):
        if not ped_detections.header.frame_id == '':
            self.ped_detection_dict[ped_detections.header.frame_id] = ped_detections

    def process_callback(self,event):
        if len(self.clusters.mean_points) == 0:
            return
        else:
            clusters = deepcopy(self.clusters)
            cluster_hit_likelihood = {label: 0.0 for label in clusters.labels}

            d = deepcopy(self.ped_detection_dict)
            for (key,value) in d.iteritems():

                ped_detection = deepcopy(value)
                self.handle_ped_detections(clusters,ped_detection,cluster_hit_likelihood)

        for label, likelihood in cluster_hit_likelihood.items():
            self.cluster_hit.likelihood = likelihood
            self.cluster_hit.ped_id = label
            self.pedestrian_cluster_id_pub.publish(self.cluster_hit)

        self.display_cluster_likelihoods(clusters, cluster_hit_likelihood)
    

    def handle_ped_detections(self, clusters, ped_detections, cluster_hit_likelihood):

        # rviz visualization for the detection vectors
        self.display_vectors(ped_detections)

        # map from cluster label to the hit likelihood
        # cluster_hit_likelihood = {label: 0 for label in clusters.labels}

        try:
            # convert the cluster information into the camera frame
            cluster_centers = [self.tf_listener.transformPoint(ped_detections.header.frame_id,PointStamped(clusters.header,input_point)).point for input_point in clusters.mean_points]
            cluster_edge1 = [self.tf_listener.transformPoint(ped_detections.header.frame_id,PointStamped(clusters.header,input_point)).point for input_point in clusters.min_points]
            cluster_edge2 = [self.tf_listener.transformPoint(ped_detections.header.frame_id,PointStamped(clusters.header,input_point)).point for input_point in clusters.max_points]
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return

        for j in xrange(len(cluster_centers)):

            # get angles of the ped detections in camera frame
            center_cluster_angle=math.atan2(cluster_centers[j].x,cluster_centers[j].z)
            edge1_cluster_angle=math.atan2(cluster_edge1[j].x,cluster_edge1[j].z)
            edge2_cluster_angle=math.atan2(cluster_edge2[j].x,cluster_edge2[j].z)
            in_view, dist = self.is_in_view(cluster_centers[j], center_cluster_angle)
            if in_view:
                # cluster_hit_likelihood[clusters.labels[j]] -= self.in_view_penalty
                if dist < self.reliable_detection_range:
                    tmp_likelihood = -self.in_view_penalty
                else:
                    tmp_likelihood = 0.0

                for i in xrange(len(ped_detections.world_vectors)):
                    currError=float("inf")
                    match=None

                    # get angles of the ped detections in camera frame
                    center_detection_angle=math.atan2(ped_detections.world_vectors[i].x,ped_detections.world_vectors[i].z)
                    left_detection_angle=math.atan2(ped_detections.left_vectors[i].x,ped_detections.left_vectors[i].z)
                    right_detection_angle=math.atan2(ped_detections.right_vectors[i].x,ped_detections.right_vectors[i].z)

                    likelihood = self.compute_likelihood(center_cluster_angle, edge1_cluster_angle, edge2_cluster_angle, center_detection_angle, left_detection_angle, right_detection_angle)
                    #debug print
                    if likelihood > 1:
                        print 'likelihood:', likelihood, 'id:', clusters.labels[j]
                    # cluster_hit_likelihood[clusters.labels[j]] += likelihood
                    tmp_likelihood += self.hit_likelihood_boost * likelihood

                # Max operation across cameras
                # if cluster_hit_likelihood[clusters.labels[j]] < tmp_likelihood:
                    # cluster_hit_likelihood[clusters.labels[j]] = tmp_likelihood
                
                cluster_hit_likelihood[clusters.labels[j]] += tmp_likelihood

        # return cluster_hit_likelihood

        # for label, likelihood in cluster_hit_likelihood.items():
        #     self.cluster_hit.likelihood = likelihood
        #     self.cluster_hit.ped_id = label
        #     self.pedestrian_cluster_id_pub.publish(self.cluster_hit)

        # self.display_cluster_likelihoods(clusters, cluster_hit_likelihood)
    
    def is_in_view(self, cluster_center, cluster_angle):
        '''
        returns a boolean which indicates whether a given cluster is in the field of 
        view of a camera.
        '''
        dist = math.sqrt((cluster_center.x)**2 + (cluster_center.z)**2)
        in_view = dist < self.max_cam_radius \
         and dist > self.min_cam_radius \
         and abs(cluster_angle) < self.cam_max_angle/2.0*numpy.pi/180
        return in_view,dist
 
    def compute_likelihood(self, center_cluster_angle, edge1_cluster_angle, edge2_cluster_angle, center_detection_angle, left_detection_angle, right_detection_angle):
        '''
        computes the cost of a given cluster with respect to a ped detection. a cluster is penalized if it is in camera range
        but does not have a detection associated with it, and is rewarded for having a detection near it
        '''
        # gaussian distance with mean of the ped_angle
        #hit_likelihood = (1 / (self.sigma * math.sqrt(2 * math.pi))) * math.exp( -1 * ((ped_center_angle - clust_center_angle) ** 2 / (2 * self.sigma ** 2)) )

        # not quite gaussian, this is stripped of the factor in front. So if a detection is perfectly aligned, this returns 1
        #hit_likelihood = math.exp( -1 * ((ped_center_angle - clust_center_angle) ** 2 / (2 * self.sigma ** 2)) )

        hit_likelihood = 0

        hit_likelihood += self.non_normalized_gaussian(center_detection_angle, center_cluster_angle, self.sigma)
        hit_likelihood += max(self.non_normalized_gaussian(left_detection_angle, edge1_cluster_angle, self.sigma), self.non_normalized_gaussian(left_detection_angle, edge1_cluster_angle, self.sigma))
        hit_likelihood += max(self.non_normalized_gaussian(right_detection_angle, edge2_cluster_angle, self.sigma), self.non_normalized_gaussian(right_detection_angle, edge2_cluster_angle, self.sigma))

        return hit_likelihood / 3 # divided by three to normalize

    def non_normalized_gaussian(self, mean, inp, var):
        '''
        returns prob value for gaussian described by var and mean. It is non normalized (no 1/sigma * math.sqrt(2 * math.pi))
        becuase I would get very large values if two vectors were very close for a variance which worked well. This is 
        probably a big hack, so maybe should be gotten rid of in the future.
        '''
        return math.exp( -1 * ((mean - inp) ** 2 / (2 * (var ** 2))) )

    def display_cluster_likelihoods(self, clusters, cluster_hit_likelihood):
        likelihoods = []
        for label, prob in cluster_hit_likelihood.items():
            likelihood = Marker()

            likelihood.type = Marker.TEXT_VIEW_FACING
            likelihood.id = label
            likelihood.ns="likelihood"
            likelihood.header.frame_id = clusters.header.frame_id
            likelihood.header.stamp=rospy.Time.now()
            likelihood.lifetime=rospy.Duration(.1)

            ind = clusters.labels.index(label)

            likelihood.pose.position.x = clusters.mean_points[ind].x
            likelihood.pose.position.y = clusters.mean_points[ind].y
            likelihood.pose.position.z = clusters.mean_points[ind].z + 1.0

            likelihood.text = 'likelihood: ' + "{0:.2f}".format(prob)

            likelihood.scale.x=.3
            likelihood.scale.y=.3
            likelihood.scale.z=.3

            if prob < 0:
                likelihood.color.r=1.0
                likelihood.color.g=0.0
                likelihood.color.b=0.0
                likelihood.color.a=1.0
            
            elif prob == 0:
                likelihood.color.r=0.0
                likelihood.color.g=0.0
                likelihood.color.b=1.0
                likelihood.color.a=1.0

            else:
                likelihood.color.r=0.0
                likelihood.color.g=1.0
                likelihood.color.b=0.0
                likelihood.color.a=1.0

            likelihood.action = Marker.ADD
            likelihoods.append(likelihood)

        self.likelihood_pub.publish(likelihoods)

    def display_hits(self, matches):
        spheres = []
        for i, match in enumerate(matches):
            sphere=Marker()

            sphere.type = Marker.SPHERE
            sphere.id = i
            sphere.ns="sphere"
            sphere.header.frame_id=self.clusters.header.frame_id
            sphere.header.stamp=rospy.Time.now()
            sphere.lifetime=rospy.Duration(.1)

            sphere.pose.position.x = match.x
            sphere.pose.position.y = match.y
            sphere.pose.position.z = match.z

            sphere.scale.x=.2
            sphere.scale.y=.2
            sphere.scale.z=.2

            sphere.color.r=1.0
            sphere.color.g=0.0
            sphere.color.b=0.0
            sphere.color.a=1.0

            sphere.action = Marker.ADD
            spheres.append(sphere)

        self.sphere_pub.publish(spheres)

    def create_arrows(self, unit_vectors, scale, frame):

        arrow_array=[]

        for i, vector in enumerate(unit_vectors):
            arrow=Marker()

            arrow.type = Marker.ARROW
            arrow.ns="arrow"
            arrow.header.frame_id=frame
            arrow.header.stamp=rospy.Time.now()

            start=Point()
            start.x=0
            start.y=0
            start.z=0

            end=Point()
            end.x=scale * (vector.x)
            end.y=scale * (vector.y)
            end.z=scale * (vector.z)

            pts=[start,end]
            arrow.points=pts

            arrow.scale.x=.03
            arrow.scale.y=.06
            arrow.scale.z=0

            arrow.color.r=0
            arrow.color.g=1.0
            arrow.color.b=0
            arrow.color.a=1.0

            arrow.id = numpy.floor(random()*10000)

            arrow.lifetime=rospy.Duration(.05)
            arrow.action = Marker.ADD
            arrow_array.append(arrow)

        return arrow_array

    def display_vectors(self, ped_detections):
        #adds unit vectors into the visualization
        ARROW_SCALE = 10
        arrow_array=[]

        arrow_array.extend(self.create_arrows(ped_detections.world_vectors, ARROW_SCALE, ped_detections.header.frame_id))
        arrow_array.extend(self.create_arrows(ped_detections.left_vectors, ARROW_SCALE, ped_detections.header.frame_id))
        arrow_array.extend(self.create_arrows(ped_detections.right_vectors, ARROW_SCALE, ped_detections.header.frame_id))

        self.arrow_pub.publish(arrow_array)


def main(args):
    rospy.init_node('cluster_classifier', anonymous=True)
    classifier = ClusterClassifier()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "something shut me down"

if __name__ == '__main__':
    main(sys.argv)
