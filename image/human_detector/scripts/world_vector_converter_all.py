#!/usr/bin/env python
import roslib
roslib.load_manifest('human_detector')
import sys
import rospy
import numpy as np
import cv2
import image_geometry
from std_msgs.msg import Float64MultiArray, Int32MultiArray, String
from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import Vector3
from cv_bridge import CvBridge, CvBridgeError
from peopledetect_class import *
from ford_msgs.msg import ped_detection, potential_detections
import time

class image_converter:

    def __init__(self):
        self.camera_dict = {}
        self.cameras = rospy.get_param("object_classifier/cameras",{})
        self.camera_type = rospy.get_param("object_classifier/camera_type","realsense")
        self.image_topic = rospy.get_param("object_classifier/image_topic","rgb/image_raw")
        self.info_topic = rospy.get_param("object_classifier/info_topic","rgb/camera_info")
        self.thresh = rospy.get_param("~score_thresh",0.05)
        # print "[world vector] cameras:",self.cameras

        for camera in self.cameras.keys():
            self.camera_dict[camera] = {}
            self.camera_dict[camera]['cam_model'] = image_geometry.PinholeCameraModel()
            self.camera_dict[camera]['detections_pub'] = rospy.Publisher("ped_detections",ped_detection, queue_size=1)
            camera_info_topic = self.camera_type+"_"+camera+"/"+self.info_topic
            self.camera_dict[camera]['camera_info_sub'] = rospy.Subscriber(camera_info_topic, \
                CameraInfo,self.camera_info_cb,camera)
            self.camera_dict[camera]['found_sub'] = rospy.Subscriber("found_detections_"+camera,potential_detections, \
                self.detection_cb,camera)

        rospy.get_param_names()
        self.ped_detections = ped_detection()
        self.frame_id = None
        #self.thresh = 0.0

    def camera_info_cb(self,data,camera):
        # data.P = [602.660095, 0.000000, 321.462415, 0.000000, \
        #             0.000000, 618.272766, 249.595349, 0.000000, \
        #             0.000000, 0.000000, 1.000000, 0.000000]
        # data.K = [608.938694, 0.000000, 322.228363, \
        #             0.000000, 615.689909, 249.707966, \
        #             0.000000, 0.000000, 1.000000]
        # data.D = [0.101957, -0.491390, -0.000885, -0.001770, 0.000000]
        # data.R = [1.000000, 0.000000, 0.000000, \
        #             0.000000, 1.000000, 0.000000, \
        #             0.000000, 0.000000, 1.000000]

        print "CAMERA", camera, "INFO CB!"

        try:
            self.camera_dict[camera]['cam_model'].fromCameraInfo(data)
            print 'camera info obtained'
        except CvBridgeError, e:
            print e

        self.camera_dict[camera]['frame_id'] = data.header.frame_id
        self.camera_dict[camera]['camera_info_sub'].unregister()

    def detection_cb(self,potential_detections,camera):

        detections = self.filter_potential_detections(potential_detections)

        found=self.reconstructBBs(detections)
        
        vectMap = self.get_detection_vectors(found)
        for v in vectMap:
            assert len(vectMap[v])==3
            for x in vectMap[v]:
                assert len(x)==2

        #####     Convert to 3d Ray #####
        center_vectors=[]
        left_vectors=[]
        right_vectors=[]
        ids=[]
        for iden in vectMap:
            ids.append(iden)

            center_vector = Vector3()
            center_xyz=self.camera_dict[camera]['cam_model'].projectPixelTo3dRay((vectMap[iden][1][0],vectMap[iden][1][1]))
            center_vector.x=center_xyz[0]
            center_vector.y=center_xyz[1]
            center_vector.z=center_xyz[2]
            center_vectors.append(center_vector)

            left_vector = Vector3()
            left_xyz=self.camera_dict[camera]['cam_model'].projectPixelTo3dRay((vectMap[iden][0][0],vectMap[iden][0][1]))
            left_vector.x=left_xyz[0]
            left_vector.y=left_xyz[1]
            left_vector.z=left_xyz[2]
            left_vectors.append(left_vector)

            right_vector = Vector3()
            right_xyz=self.camera_dict[camera]['cam_model'].projectPixelTo3dRay((vectMap[iden][2][0],vectMap[iden][2][1]))
            right_vector.x=right_xyz[0]
            right_vector.y=right_xyz[1]
            right_vector.z=right_xyz[2]
            right_vectors.append(right_vector)
        #fill ped_detections
        ped_detections = ped_detection()
        ped_detections.world_vectors=center_vectors
        ped_detections.left_vectors=left_vectors
        ped_detections.right_vectors=right_vectors
        ped_detections.ids=ids
        ped_detections.header.stamp=rospy.get_rostime()
        # ped_detections.header.frame_id='/camera_rgb_optical_frame'
        
        if self.camera_dict[camera]['frame_id'] is not None:
            ped_detections.header.frame_id=self.camera_dict[camera]['frame_id']

        
        # print 'now: ', rospy.Time.now(), 'stamp: ',potential_detections.header.stamp, 'diff: ', (rospy.Time.now() - potential_detections.header.stamp).to_sec()

        #####     Publish     #####
        try:
            self.camera_dict[camera]['detections_pub'].publish(ped_detections)
        except CvBridgeError, e:
            print "failure publishing detection vectors,",e

    #reconstruct BBs from the Int32MultiArray message
    def reconstructBBs(self,deconstructed_found):
        found=[]
        bb=[]
        for i in range(len(deconstructed_found)):
            if i%4==0 and i!=0:
                found.append(bb)
                bb=[deconstructed_found[i]]
            else:
                bb.append(deconstructed_found[i])
        if len(bb)!=0:
            found.append(bb)
        return found

    #gets the pixels of important points from each bounding box
    def get_detection_vectors(self,found):
        vectMap= {}
        detection_vects=[]
        i=0
        for detection in found:
                        
            pad_w, pad_h = int(0.25*detection[2]), int(0.05*detection[3])
                        
            x_center=(detection[0]+(detection[2]/2))
            y_center=(detection[1]+(detection[3]/2))
            xy_center=(x_center,y_center)

            x_left=detection[0]+pad_w
            y_left=(detection[1]+(detection[3]/2))
            xy_left=(x_left,y_left)

            x_right=(detection[0]+detection[2]-pad_w)
            y_right=(detection[1]+(detection[3]/2))
            xy_right=(x_right,y_right)

            detection_vects.append(xy_left)
            detection_vects.append(xy_center)
            detection_vects.append(xy_right)

            vectMap[i]=detection_vects
            detection_vects=[]
            i+=1
        return vectMap

    def filter_potential_detections(self, potential_detections):
        filtered_detection = []
        for i, confidence in enumerate(potential_detections.scores):
            if confidence > self.thresh:
                filtered_detection.extend(potential_detections.detections[i * 4: (i + 1) * 4])
        
        return filtered_detection

def main(args):
    rospy.init_node('image_converter', anonymous=True)
    ic = image_converter()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
