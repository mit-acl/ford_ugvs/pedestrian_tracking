#!/usr/bin/env python
import roslib
import rospy
import sys
import math
import random
import numpy
import time
from os import path, makedirs
from ford_msgs.msg import Clusters
from std_msgs.msg import UInt32
from geometry_msgs.msg import Point, PointStamped
from visualization_msgs.msg import MarkerArray, Marker

class StampedTrajectory(object):
    def __init__(self,time,cluster_id,point):
        self.cluster_id=cluster_id
        self.time=time
        self.point=point

class cluster_saver:

    def __init__(self):
        self.pedestrian_cluster_id_sub = rospy.Subscriber("pedestrian_cluster_id",UInt32, self.pedestrian_cluster_id_callback)
        self.clusters_sub = rospy.Subscriber("/cluster/output/clusters", Clusters,self.clusters_callback)
        self.line_pub = rospy.Publisher("paths",MarkerArray, queue_size=10)
        self.directory = rospy.get_param('~output_file_directory', '')
        self.file_name = self.directory+'/trajectories_'+time.strftime('%m-%d-%Y_%H:%M:%S')+'.txt'
        self.cluster_life_time = rospy.get_param('~cluster_life_time', 60)
        self.number_of_ped_detections = rospy.get_param('~number_of_ped_detections', 3)
        self.frame_id = ''
        self.isPedestrianMap={}
        self.trajectoryMap = {}
        self.colorMap={}
        if not path.exists(self.directory):
    		makedirs(self.directory)
        with open(self.file_name, 'w') as f:
            f.write('time, cluster_id, x, y, z \n')

    def pedestrian_cluster_id_callback(self,pedestrian_cluster_id):
        # Adds each incoming pedestrian cluster id message to the map and counts how many have been received
        if pedestrian_cluster_id.data not in self.isPedestrianMap:
            self.isPedestrianMap[pedestrian_cluster_id.data]=0
        self.isPedestrianMap[pedestrian_cluster_id.data]+=1
                                
    def get_pedestrian_count_by_id(self,cluster_id):
        # Create a unique color for each trajectory
        if cluster_id not in self.isPedestrianMap:
            return 0
        return self.isPedestrianMap[cluster_id]

    def clusters_callback(self,clusters):
        # Add mean points for each id to path array
        for i in range(len(clusters.labels)):
            if clusters.labels[i] not in self.trajectoryMap:
                self.trajectoryMap[clusters.labels[i]] = []
            self.trajectoryMap[clusters.labels[i]].append(StampedTrajectory(clusters.header.stamp.to_sec(),clusters.labels[i],clusters.mean_points[i]))

        # Manage the size of the cluster map
        self.remove_old_data()
 
        # Send path messages for rviz
        self.frame_id = clusters.header.frame_id
        self.display_markers()

    def remove_old_data(self):
        current_time = rospy.Time.now().to_sec()
        ids_to_drop = []
        for cluster_id in self.trajectoryMap:
            # Check if the last time the cluster was seen is older than the threshold (in nano seconds)
            if current_time - self.trajectoryMap[cluster_id][-1].time > self.cluster_life_time:
                ids_to_drop.append(cluster_id)
        for cluster_id in ids_to_drop:
            # Handle the case for non-pedestrians
            if self.get_pedestrian_count_by_id(cluster_id) < self.number_of_ped_detections:
                # Pop off the id's from each map and do nothin with them
                self.trajectoryMap.pop(cluster_id)
                if cluster_id in self.isPedestrianMap:
                    self.isPedestrianMap.pop(cluster_id)
            else: 
                # Pop off the cluster and put the trajectory into a data file
                self.isPedestrianMap.pop(cluster_id)
                self.colorMap.pop(cluster_id)
                with open(self.file_name, 'a') as f:
                    for data in self.trajectoryMap.pop(cluster_id):
                        f.write(str([data.time, data.cluster_id, data.point.x, data.point.y, data.point.z]).strip('[]')+'\n')

    def get_color_by_id(self,cluster_id):
        # Create a unique color for each trajectory
        if cluster_id not in self.colorMap:
            self.colorMap[cluster_id]=[random.random(),random.random(),random.random()]
        return self.colorMap[cluster_id]

    def display_markers(self):
        # Create a marker array for rviz
        line_array=[]
        for cluster_id in self.trajectoryMap:
            if self.get_pedestrian_count_by_id(cluster_id) >= self.number_of_ped_detections:
                line=Marker()
                line.type = Marker.LINE_STRIP
                line.id=cluster_id
                line.ns="LINE_STRIP"
                line.header.frame_id=self.frame_id
                line.header.stamp=rospy.Time.now()
                line.points=[data.point for data in self.trajectoryMap[cluster_id]]
                line.scale.x=.03
                color = self.get_color_by_id(cluster_id)
                line.color.r=color[0]
                line.color.g=color[1]
                line.color.b=color[2]
                line.color.a=1.0
                line.lifetime=rospy.Duration(.5)
                line.action = Marker.ADD
                line_array.append(line)
        self.line_pub.publish(line_array)

    def shutting_down(self,message):
        for cluster_id in self.trajectoryMap:
            if self.get_pedestrian_count_by_id(cluster_id) >= self.number_of_ped_detections:
                with open(self.file_name, 'a') as f:
                    for data in self.trajectoryMap[cluster_id]:
                        f.write(str([data.time, data.cluster_id, data.point.x, data.point.y, data.point.z]).strip('[]')+'\n')

def main(args):
    rospy.init_node('cluster_saver', anonymous=True)
    cluster_saver_class = cluster_saver()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "something shut me down"
    rospy.on_shutdown(cluster_saver_class.shutting_down)

if __name__ == '__main__':
    main(sys.argv)