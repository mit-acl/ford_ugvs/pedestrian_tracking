#!/usr/bin/env python
import rospy
import os
import numpy as np
import pycurl
import json
from StringIO import StringIO
from datetime import date
from couchdb import Server

from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec
from std_msgs.msg import Float32

class DbLogger(object):
    def __init__(self):

        # Create unique run_id for the data
        self.run_id = rospy.Time.now().secs
        rospy.loginfo("[DbLogger] run_id: %s" %(self.run_id))

        # Setup server
        db_server = rospy.get_param('~db_server','http://swarm:mobility@veh10.mit.edu:5984') #Should default be localhost?
        server = Server(db_server)

        # Setup data database
        db_name = rospy.get_param('~db_name','')
        if not db_name:
            db_name = date.today().strftime("data_%m-%d-%Y")
        if db_name in server:
            db = server[db_name]
        else:
            db = server.create(db_name)

        #Setup design docs
        design_docs = [{"_id": "_design/dfilter","type": "design_doc","filters": {"not_deleted": "function(doc, req) {return !doc._deleted;}"}}
                      ,{"_id": "_design/updater","type": "design_doc","language": "javascript","updates": {"handle_data": "function(doc, req) {data = JSON.parse(req.body); if (!doc) {new_doc = { _id: data._id, type: data.type, vehicle_id: data.vehicle_id, ped_id: data.ped_id, run_id: data.run_id, map_name: data.map_name, publish_time: data.publish_time, path_time_list: data.path_time_list, path_x_list: data.path_x_list, path_y_list: data.path_y_list, path_theta_list: data.path_theta_list, easting_list: data.easting_list, northing_list: data.northing_list, utm_theta_list: data.utm_theta_list, start_time: data.path_time_list[0], end_time: data.path_time_list[data.path_time_list.length - 1], }; var message = 'created new doc'; return [new_doc, message]; } else {doc.path_time_list = doc.path_time_list.concat(data.path_time_list); doc.path_x_list = doc.path_x_list.concat(data.path_x_list); doc.path_y_list = doc.path_y_list.concat(data.path_y_list); doc.path_theta_list = doc.path_theta_list.concat(data.path_theta_list); doc.easting_list = doc.easting_list.concat(data.easting_list); doc.northing_list = doc.northing_list.concat(data.northing_list); doc.utm_theta_list = doc.utm_theta_list.concat(data.utm_theta_list); doc.start_time = doc.path_time_list[0]; doc.end_time = doc.path_time_list[doc.path_time_list.length - 1]; var message = 'updated data'; return [doc, message]; } }"}}]
        for doc in design_docs:
            if doc['_id'] not in db:
                db.save(doc)

        #Setup replicator
        replicator_db = server["_replicator"]
        if "backup_"+ db_name not in replicator_db:
            new_replicator_doc = {"_id": "backup_"+db_name, "source": "http://swarm:mobility@ride.mit.edu:5984/"+db_name, "target": "http://swarm:mobility@veh9.mit.edu:5984/"+db_name+"_backup", "create_target": True, "continuous": True, "filter": "dfilter/not_deleted", "owner": "swarm"}
            replicator_db.save(new_replicator_doc)

        #Setup security
        if ("_security" not in db) or ("admins" not in db["_security"]):
            # security_doc = {"_id":"_security","admins": {"names": ["swarm"],"roles": []}}
            # db.save(security_doc)
            db.resource.put("_security",{u'admins': {u'names': [u'swarm'], u'roles': [] }})

        #Full url for pushing data
        update_doc_name = '_design/updater/_update/handle_data'
        self.url = db_server + '/' + db_name + '/' + update_doc_name + '/'
        rospy.loginfo("[DbLogger] Logging to " + db_name + '/' + update_doc_name)

        # Get Grid to UTC transform
        map_transform_data = rospy.get_param("~map_transform_data",'/home/swarm/ford_ws/src/ford/ford_ros/maps/stata_aug_2015.py')
        self.grid_to_utm = np.asarray(eval(open(map_transform_data).read()))
        self.grid_to_utm_theta = np.arccos(self.grid_to_utm[0,0]/np.linalg.norm(self.grid_to_utm[0,0:2],2))
        self.map_name = os.path.basename(map_transform_data).strip('.py')

        # Get vehicle id      
        self.veh_id = os.environ.get('VEHICLE_ID') #Will be empty if not set.

        self.localized = 0
        self.localized_array = [False]*rospy.get_param("~num_bad_localiztions",10)
        self.localized_threshold = rospy.get_param("~localized_threshold",0.9)

        # TODO: catch exceptions?
        rospy.loginfo("[DbLogger] Vehicle_id: %s"%(self.veh_id))

        self.sub_ped_diff = rospy.Subscriber("~ped_diff",PedTrajVec, self.cbPedDiff)
        self.sub_veh_diff = rospy.Subscriber("~veh_diff",PedTraj, self.cbVehDiff)
        self.sub_localized = rospy.Subscriber("localized",Float32, self.cbLocalized)

    def cbLocalized(self,localized):
        self.localized = localized.data
        self.localized_array.pop(0)
        self.localized_array.append(localized.data>=self.localized_threshold)

    def cbPedDiff(self,ped_traj_vec):
        if any(self.localized_array):
            for ped_traj in ped_traj_vec.ped_traj_vec:
                dbDoc = self.toDbDoc(ped_traj)
                response = self.postData(dbDoc)
                print 'pedestrian: ' +  response
        else:
            print "Not localized: ", self.localized, ", skipping pedstrian database update"

    def cbVehDiff(self,veh_traj):
        if any(self.localized_array):
            dbDoc = self.toDbDoc(veh_traj,'vehicle')
            response = self.postData(dbDoc)
            print 'vehicle: ' + response
        else:
            print "Not localized: ", self.localized, ", skipping vehicle database update"


    def toDbDoc(self,ped_traj,data_type='pedestrian'):
        # Convert a ped_traj to a db document (dictionary)
        ped_time = []; ped_x = []; ped_y = []; ped_theta = [];
        for poseStamped in ped_traj.traj:
            ped_time.append(poseStamped.header.stamp.to_sec())
            ped_x.append(poseStamped.pose.x)
            ped_y.append(poseStamped.pose.y)
            ped_theta.append(poseStamped.pose.theta)
        easting, northing, utm_theta = self.xy_to_utm(ped_x, ped_y, ped_theta)
        
        dbDoc ={
        '_id':str(self.run_id)+'_'+str(self.veh_id)+'_'+str(ped_traj.ped_id),
        'type':data_type,
        'vehicle_id':self.veh_id,
        'ped_id':ped_traj.ped_id,
        'run_id':self.run_id,
        'path_time_list':ped_time,
        'path_x_list':ped_x,
        'path_y_list':ped_y,
        'path_theta_list':ped_theta,
        'easting_list':easting,
        'northing_list':northing,
        'utm_theta_list':utm_theta,
        'map_name':self.map_name,
        'publish_time':[rospy.Time.now().secs,rospy.Time.now().nsecs]
        }
        return dbDoc

    def postData(self,dbDoc):
        buffer = StringIO()
        c = pycurl.Curl()
        c.setopt(pycurl.URL, '%s' % self.url+ dbDoc['_id'] )
        c.setopt(pycurl.HTTPHEADER, ['Accept: application/json', 'Content-Type: application/json'])
        c.setopt(pycurl.POST, 1)
        c.setopt(pycurl.POSTFIELDS, json.dumps(dbDoc))
        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        c.close()
        return buffer.getvalue()

    def xy_to_utm(self,x,y,theta):
        xy = np.array([x,y,np.ones(len(x))])
        en = np.dot(self.grid_to_utm.transpose(),xy)
        easting = np.divide(en[0],en[2])
        northing = np.divide(en[1],en[2])
        utm_theta = theta - self.grid_to_utm_theta
        return list(easting), list(northing), list(utm_theta)

                
if __name__ == '__main__':
    rospy.init_node('database_logger',anonymous=False)
    rospy.sleep(0.1)
    db_logger = DbLogger()
    rospy.spin()