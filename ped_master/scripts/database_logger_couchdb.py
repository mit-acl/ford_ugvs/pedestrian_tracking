#!/usr/bin/env python
import rospy
import os
import numpy as np
from datetime import date
import requests
from couchdb import Server

from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec
from std_msgs.msg import Float32

# This file uses the built in couchdb module to call update requests, but it doesn't concatenate data properly so its best to continue using the original.

class DbLogger(object):
    def __init__(self,run_id):
        # rospack = rospkg.RosPack()
        self.run_id = run_id
        rospy.loginfo("[DbLogger] run_id: %s" %(self.run_id))

        # Set database parameters
        db_server = rospy.get_param('~db_server','http://swarm:mobility@veh10.mit.edu:5984') #Should default be localhost?
        db_name = rospy.get_param('~db_name','')
        if not db_name:
            r = requests.get("http://veh10.mit.edu:5000/update_db") # Update server to make a new db if necessary
            db_name = date.today().strftime("data_%m-%d-%Y")
        self.update_doc_name = rospy.get_param('~update_doc_name','updater/handle_data') #What's the default name of db?

        server = Server(db_server)
        self.db = server[db_name]

        rospy.loginfo("[DbLogger] Logging to " + db_name)

        # Get Grid to UTC transform
        map_transform_data = rospy.get_param("~map_transform_data",'/home/swarm/ford_ws/src/ford/ford_ros/maps/stata_aug_2015.py')
        self.grid_to_utm = np.asarray(eval(open(map_transform_data).read()))
        self.grid_to_utm_theta = np.arccos(self.grid_to_utm[0,0]/np.linalg.norm(self.grid_to_utm[0,0:2],2))
        self.map_name = os.path.basename(map_transform_data).strip('.py')

        # Get vehicle id      
        self.veh_id = os.environ.get('VEHICLE_ID') #Will be empty if not set.

        self.localized = 0
        self.localized_array = [False]*rospy.get_param("~num_bad_localiztions",10)
        self.localized_threshold = rospy.get_param("~localized_threshold",0.9)

        # TODO: catch exceptions?
        rospy.loginfo("[DbLogger] Vehicle_id: %s"%(self.veh_id))

        self.sub_ped_diff = rospy.Subscriber("~ped_diff",PedTrajVec, self.cbPedDiff)
        self.sub_veh_diff = rospy.Subscriber("~veh_diff",PedTraj, self.cbVehDiff)
        self.sub_localized = rospy.Subscriber("localized",Float32, self.cbLocalized)

    def cbLocalized(self,localized):
        self.localized = localized.data
        self.localized_array.pop(0)
        self.localized_array.append(localized.data>=self.localized_threshold)

    def cbPedDiff(self,ped_traj_vec):
        if any(self.localized_array):
            for ped_traj in ped_traj_vec.ped_traj_vec:
                dbDoc = self.toDbDoc(ped_traj)
                headers, body = self.db.update_doc(self.update_doc_name,dbDoc['_id'],**dbDoc)
                print 'pedestrian: ' +  body.read()
        else:
            print "Not localized: ", self.localized, ", skipping pedstrian database update"

    def cbVehDiff(self,veh_traj):
        if any(self.localized_array):
            dbDoc = self.toDbDoc(veh_traj,'vehicle')
            print dbDoc
            headers, body = self.db.update_doc(self.update_doc_name,dbDoc['_id'],**dbDoc)
            print 'vehicle: ' + body.read()
        else:
            print "Not localized: ", self.localized, ", skipping vehicle database update"


    def toDbDoc(self,ped_traj,data_type='pedestrian'):
        # Convert a ped_traj to a db document (dictionary)
        ped_time = []; ped_x = []; ped_y = []; ped_theta = [];
        for poseStamped in ped_traj.traj:
            ped_time.append(poseStamped.header.stamp.to_sec())
            ped_x.append(poseStamped.pose.x)
            ped_y.append(poseStamped.pose.y)
            ped_theta.append(poseStamped.pose.theta)
        easting, northing, utm_theta = self.xy_to_utm(ped_x, ped_y, ped_theta)
        
        dbDoc ={
        '_id':str(self.run_id)+'_'+str(self.veh_id)+'_'+str(ped_traj.ped_id),
        'type':data_type,
        'vehicle_id':self.veh_id,
        'ped_id':ped_traj.ped_id,
        'run_id':self.run_id,
        'path_time_list':ped_time,
        'path_x_list':ped_x,
        'path_y_list':ped_y,
        'path_theta_list':ped_theta,
        'easting_list':easting,
        'northing_list':northing,
        'utm_theta_list':utm_theta,
        'map_name':self.map_name,
        'publish_time':[rospy.Time.now().secs,rospy.Time.now().nsecs]
        }
        return dbDoc

    def xy_to_utm(self,x,y,theta):
        xy = np.array([x,y,np.ones(len(x))])
        en = np.dot(self.grid_to_utm.transpose(),xy)
        easting = np.divide(en[0],en[2])
        northing = np.divide(en[1],en[2])
        utm_theta = theta - self.grid_to_utm_theta
        return list(easting), list(northing), list(utm_theta)

                
if __name__ == '__main__':
    rospy.init_node('database_logger',anonymous=False)
    rospy.sleep(0.1)
    run_id = rospy.Time.now().secs
    db_logger = DbLogger(run_id)
    rospy.spin()