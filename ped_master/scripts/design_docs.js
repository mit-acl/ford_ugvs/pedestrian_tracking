function(doc, req) {
    data = JSON.parse(req.body);
    if (!doc) {
        new_doc = { _id: data._id, type: data.type, vehicle_id: data.vehicle_id, ped_id: data.ped_id, run_id: data.run_id, map_name: data.map_name, publish_time: data.publish_time, path_time_list: data.path_time_list, path_x_list: data.path_x_list, path_y_list: data.path_y_list, path_theta_list: data.path_theta_list, easting_list: data.easting_list, northing_list: data.northing_list, utm_theta_list: data.utm_theta_list, start_time: data.path_time_list[0], end_time: data.path_time_list[data.path_time_list.length - 1], };
        var message = 'created new doc';
        return [new_doc, message];
    } else {
        doc.path_time_list = doc.path_time_list.concat(data.path_time_list);
        doc.path_x_list = doc.path_x_list.concat(data.path_x_list);
        doc.path_y_list = doc.path_y_list.concat(data.path_y_list);
        doc.path_theta_list = doc.path_theta_list.concat(data.path_theta_list);
        doc.easting_list = doc.easting_list.concat(data.easting_list);
        doc.northing_list = doc.northing_list.concat(data.northing_list);
        doc.utm_theta_list = doc.utm_theta_list.concat(data.utm_theta_list);
        doc.start_time = doc.path_time_list[0];
        doc.end_time = doc.path_time_list[doc.path_time_list.length - 1];
        var message = 'updated data';
        return [doc, message];
    }
}
