#!/usr/bin/env python
import rospy
import os
import numpy as np

from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec
from std_msgs.msg import Float32

class FileLogger(object):
    def __init__(self,run_id):
        # rospack = rospkg.RosPack()
        self.run_id = run_id
        rospy.loginfo("[FileLogger] run_id: %s" %(self.run_id))
        
        self.sub_ped_diff = rospy.Subscriber("~ped_diff",PedTrajVec, self.cbPedDiff)
        self.sub_veh_diff = rospy.Subscriber("~veh_diff",PedTraj, self.cbVehDiff)
        self.sub_localized = rospy.Subscriber("localized",Float32, self.cbLocalized)

        # Get Grid to UTC transform
        map_transform_data = rospy.get_param("~map_transform_data",'/home/swarm/ford_ws/src/ford/ford_ros/maps/stata_aug_2015.py')
        self.grid_to_utm = np.asarray(eval(open(map_transform_data).read()))
        self.grid_to_utm_theta = np.arccos(self.grid_to_utm[0,0]/np.linalg.norm(self.grid_to_utm[0,0:2],2))
        self.map_name = os.path.basename(map_transform_data).strip('.py')

        self.localized = 0

        # Get vehicle id  
        self.veh_id = os.environ.get('VEHICLE_ID') #Will be empty if not set.
        if not self.veh_id:
            self.veh_id = ''

        # Set default path
        self.directory = rospy.get_param('~output_file_directory',os.environ['HOME']+'/Desktop/trajectories')
        self.file_name = self.directory+'/trajectories_'+self.veh_id+'_'+str(self.run_id)+'_'+self.map_name+'.txt'
        rospy.loginfo("[FileLogger] Logging to " + self.file_name)

        # Open file and prepare to write
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        with open(self.file_name, 'w') as f:
            f.write('time, cluster_id, x, y, theta, easting, northing, utm_theta, type, localized \n')

        # TODO: catch exceptions?

    def cbLocalized(self,localized):
        self.localized = localized

    def cbPedDiff(self,ped_traj_vec):
        for ped_traj in ped_traj_vec.ped_traj_vec:
            ped_traj.type = 'pedestrian'
            self.writeToFile(ped_traj)

    def cbVehDiff(self,veh_traj):
        veh_traj.type='vehicle'
        self.writeToFile(veh_traj)

    def writeToFile(self,traj):
            ped_id = traj.ped_id
            with open(self.file_name, 'a') as f:
                for poseStamped in traj.traj:
                    easting, northing, utm_theta = self.xy_to_utm([poseStamped.pose.x], [poseStamped.pose.y], [poseStamped.pose.theta])
                    f.write(str([poseStamped.header.stamp.to_sec(), ped_id, poseStamped.pose.x, poseStamped.pose.y, poseStamped.pose.theta, easting[0], northing[0], utm_theta[0], traj.type, self.localized]).strip('[]')+'\n')

    def xy_to_utm(self,x,y,theta):
        xy = np.array([x,y,np.ones(len(x))])
        en = np.dot(self.grid_to_utm.transpose(),xy)
        easting = np.divide(en[0],en[2])
        northing = np.divide(en[1],en[2])
        utm_theta = theta - self.grid_to_utm_theta
        return list(easting), list(northing), list(utm_theta)

                
if __name__ == '__main__':
    rospy.init_node('file_logger',anonymous=False)
    rospy.sleep(0.1)
    run_id = rospy.Time.now().secs
    file_logger = FileLogger(run_id)
    rospy.spin()