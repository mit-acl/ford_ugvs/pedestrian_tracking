#!/usr/bin/env python
import rospy
from visualization_msgs.msg import Marker

if __name__ == '__main__':
    rospy.init_node('mesh_tester',anonymous=False)
    rospy.sleep(0.1)

    pub_mesh = rospy.Publisher('mesh_test',Marker,queue_size=1,latch=True)

    mesh_marker = Marker()
    mesh_marker.header.frame_id = "map"
    mesh_marker.header.stamp = rospy.Time.now()
    mesh_marker.ns = "mesh_tester";
    mesh_marker.id = 0
    mesh_marker.lifetime = rospy.Duration(0.0)
    mesh_marker.type = Marker.MESH_RESOURCE
    mesh_marker.pose.position.x = 38;
    mesh_marker.pose.position.y = 117.5; 
    mesh_marker.pose.position.z = 0;
    mesh_marker.pose.orientation.z = 0.57;
    mesh_marker.pose.orientation.w = 1;
    mesh_marker.scale.x = 5.5
    mesh_marker.scale.y = 5.5
    mesh_marker.scale.z = 1.0
    mesh_marker.mesh_resource = "package://ped_master/resource/map.dae"
    mesh_marker.mesh_use_embedded_materials = True
    mesh_marker.color.r = 0.0
    mesh_marker.color.g = 0.0
    mesh_marker.color.b = 0.0
    mesh_marker.color.a = 0.0

    pub_mesh.publish(mesh_marker)
    rospy.spin()