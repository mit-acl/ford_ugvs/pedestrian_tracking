#!/usr/bin/env python
import rospy
import time
from geometry_msgs.msg import Point32
from sensor_msgs.msg import PointCloud
from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec

class Ped2Costmap():
    def __init__(self):
        rospy.loginfo("[Ped2Costmap] Started.")
        self.frame_id = rospy.get_param('~frame_id','map')
        self.radius = rospy.get_param('~inflation_radius',0.1)
        self.ped_points = dict()
        self.pub_period = 0.1

        self.sub_ped = rospy.Subscriber("~ped",PedTrajVec, self.cbPed)
        self.pub_cloud = rospy.Publisher("~cloud",PointCloud, queue_size=1)
        self.timer = rospy.Timer(rospy.Duration(self.pub_period), self.cbPubMarker)

    def cbPed(self,msg):
        for ped in msg.ped_traj_vec:
            ped_id = ped.ped_id
            points_array = []
            update_time = time.time()
            for pt in ped.traj:
                points_array.append(Point32(x=pt.pose.x,y=pt.pose.y,z=0.5))
                points_array.append(Point32(x=pt.pose.x+self.radius,y=pt.pose.y,z=0.5))
                points_array.append(Point32(x=pt.pose.x-self.radius,y=pt.pose.y,z=0.5))
                points_array.append(Point32(x=pt.pose.x,y=pt.pose.y+self.radius,z=0.5))
                points_array.append(Point32(x=pt.pose.x,y=pt.pose.y-self.radius,z=0.5))
            self.ped_points[ped_id] = [update_time, points_array]


    def cbPubMarker(self,event):
        cloud = PointCloud()
        cloud.header.frame_id = self.frame_id
        cloud.header.stamp = rospy.Time.now()
        cur_time = time.time()
        remove_ids = []
        for ped_id, points_array_struct in self.ped_points.iteritems():
            update_time = points_array_struct[0]
            if cur_time - update_time < self.pub_period*2:
                cloud.points += points_array_struct[1]
            else:
                remove_ids.append(ped_id)

        for ped_id in remove_ids:
            del self.ped_points[ped_id]

        self.pub_cloud.publish(cloud)



                
if __name__ == '__main__':
    rospy.init_node('ped2costmap',anonymous=False)
    ped2costmap = Ped2Costmap()
    rospy.spin()