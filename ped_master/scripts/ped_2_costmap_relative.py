#!/usr/bin/env python
import rospy
import time
from geometry_msgs.msg import Point32, Vector3
from sensor_msgs.msg import PointCloud
from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec
import numpy as np

class Ped2Costmap():
    def __init__(self):
        rospy.loginfo("[Ped2Costmap] Started.")
        self.frame_id = rospy.get_param('~frame_id','map')
        self.radius = rospy.get_param('~inflation_radius',0.1)
        self.ped_speed_threshold = rospy.get_param('~ped_speed_threshold',0.5)
        self.ped_points = dict()
        self.pub_period = 0.1
        self.veh_vel = None

        self.sub_ped = rospy.Subscriber("~ped",PedTrajVec, self.cbPed)
        self.sub_vel = rospy.Subscriber("~vel",Vector3, self.cbVel)
        self.pub_cloud = rospy.Publisher("~cloud",PointCloud, queue_size=1)
        self.timer = rospy.Timer(rospy.Duration(self.pub_period), self.cbPubMarker)

    def cbVel(self,msg):
        self.veh_vel = msg

    def cbPed(self,msg):
        if self.veh_vel is None:
            return
        for ped in msg.ped_traj_vec:
            ped_id = ped.ped_id
            update_time = time.time()
            first_pt = np.array([ped.traj[0].pose.x, ped.traj[0].pose.y])
            last_pt = np.array([ped.traj[-1].pose.x, ped.traj[-1].pose.y])

            # hard coded, 3.0 is num_steps in acl_gp
            finalTime = 0.5
            ped_speed = np.linalg.norm(last_pt-first_pt) / finalTime
            if ped_speed<self.ped_speed_threshold:
                # print "Skipping ped, speed too low, speed: ", ped_speed
                return

            # print "Using ped, speed: ", ped_speed

            dt = 0
            points_array = []   
            for pt in ped.traj:
                pt_np = np.array([pt.pose.x, pt.pose.y])
                dt = min(finalTime, np.linalg.norm(pt_np-first_pt) / max(ped_speed, 1e-6)) 
                # print 'ped_spd:',ped_speed,'veh_spd:',self.veh_vel.x,',',self.veh_vel.y
                pt_x = pt.pose.x - dt * self.veh_vel.x
                pt_y = pt.pose.y - dt * self.veh_vel.y
                # print pt.pose.y,dt * self.veh_vel.y
                # print dt * self.veh_vel.x, dt * self.veh_vel.y
                points_array.append(Point32(x=pt_x,y=pt_y,z=0.5))
                points_array.append(Point32(x=pt_x+self.radius,y=pt_y,z=0.5))
                points_array.append(Point32(x=pt_x-self.radius,y=pt_y,z=0.5))
                points_array.append(Point32(x=pt_x,y=pt_y+self.radius,z=0.5))
                points_array.append(Point32(x=pt_x,y=pt_y-self.radius,z=0.5))
            self.ped_points[ped_id] = [update_time, points_array]


    def cbPubMarker(self,event):
        cloud = PointCloud()
        cloud.header.frame_id = self.frame_id
        cloud.header.stamp = rospy.Time.now()
        cur_time = time.time()
        remove_ids = []
        for ped_id, points_array_struct in self.ped_points.iteritems():
            update_time = points_array_struct[0]
            if cur_time - update_time < self.pub_period*2:
                cloud.points += points_array_struct[1]
            else:
                remove_ids.append(ped_id)

        for ped_id in remove_ids:
            del self.ped_points[ped_id]

        self.pub_cloud.publish(cloud)



                
if __name__ == '__main__':
    rospy.init_node('ped2costmap',anonymous=False)
    ped2costmap = Ped2Costmap()
    rospy.spin()