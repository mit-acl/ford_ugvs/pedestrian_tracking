#!/usr/bin/env python
import rospy
import time, sys
# from os import path, makedirs
import os, imp
# import rospkg
# from ConnectionToDatabase import *
import numpy as np
# import couchdb
import pycurl
import json
from StringIO import StringIO

from ford_msgs.msg import Pose2DStamped, PedTraj, PedTrajVec

class PedLogger(object):
    def __init__(self,run_id):
        # rospack = rospkg.RosPack()
        self.run_id = run_id
        rospy.loginfo("[PedLogger] run_id: %s" %(self.run_id))

        # Set default path
        self.directory = rospy.get_param('~output_file_directory',os.environ['HOME']+'/Desktop/trajectories')
        self.file_name = self.directory+'/trajectories_'+time.strftime('%m-%d-%Y_%H:%M:%S')+'.txt'
        # self.directory = rospack.get_path('ford_msgs') + "/log"
        
        # Open database        
        db_server = rospy.get_param('~db_server','http://swarm:mobility@veh10.mit.edu:5984') #Should default be localhost?
        db_name = rospy.get_param('~db_name','test') #What's the default name of db?
        update_doc_name = rospy.get_param('~update_doc_name','_design/updater/_update/handle_data') #What's the default name of db?
        self.url = db_server + '/' + db_name + '/' + update_doc_name + '/';
        print self.url
        # self.db = self.connectToDataBase(self.db_server,self.db_name)
        # rospy.loginfo("[PedLogger] Logging to server: %s name: %s"%(self.db_server,self.db_name))
        
        self.sub_ped_diff = rospy.Subscriber("~ped_diff",PedTrajVec, self.cbPedDiff)
        self.sub_veh_diff = rospy.Subscriber("~veh_diff",PedTraj, self.cbVehDiff)

        rospy.loginfo("[PedLogger] Logging to " + self.file_name)

        # Get Grid to UTC transform
        map_transform_data = rospy.get_param("~map_transform_data",'/home/swarm/ford_ws/src/ford/ford_ros/maps/stata_aug_2015.py')
        self.grid_to_utm = np.asarray(eval(open(map_transform_data).read()))
        self.map_name = os.path.basename(map_transform_data).strip('.py')

        # Open file and prepare to write
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        with open(self.file_name, 'w') as f:
            f.write('time, cluster_id, x, y, easting, northing \n')

        # Get vehicle id
        # self.veh_id = rospy.get_param('~vehicle_id',"") #TODO edit launch file        
        self.veh_id = os.environ.get('VEHICLE_ID') #Will be empty if not set.

        # TODO: catch exceptions?
        rospy.loginfo("[PedLogger] Vehicle_id: %s"%(self.veh_id))

        
    def toDbDoc(self,ped_traj,data_type='pedestrian'):
        # Convert a ped_traj to a db document (dictionary)
        ped_time = []; ped_x = []; ped_y = []
        for poseStamped in ped_traj.traj:
            ped_time.append(poseStamped.header.stamp.to_sec())
            ped_x.append(poseStamped.pose.x)
            ped_y.append(poseStamped.pose.y)
        easting, northing = self.xy_to_utm(ped_x,ped_y)
        
        dbDoc ={
        '_id':str(self.run_id)+'_'+str(self.veh_id)+'_'+str(ped_traj.ped_id),
        'type':data_type, #Should this be 'label'?
        'vehicle_id':self.veh_id,
        'ped_id':ped_traj.ped_id,
        'run_id':self.run_id,
        # 'ped_uuid':ped_uuid,
        # 'start_time':ped_time[0],
        # 'end_time':ped_time[-1],
        'path_time_list':ped_time,
        'path_x_list':ped_x,
        'path_y_list':ped_y,
        'easting_list':easting,
        'northing_list':northing,
        'map_name':self.map_name,
        'publish_time':[rospy.Time.now().secs,rospy.Time.now().nsecs]
        }
        return dbDoc

    # def connectToDataBase(self,database_server, database_name):
    #     # Connect to couchdb and return the database object
    #     server = couchdb.Server(database_server)
    #     if(server.__contains__(database_name)):
    #         database = server.__getitem__(database_name)
    #     else:
    #         server.create(database_name)
    #         database = server.__getitem__(database_name)

    #     return database

    def cbPedDiff(self,ped_traj_vec):
        self.writeToFile(ped_traj_vec)
        self.writeToDatabase(ped_traj_vec)

    def cbVehDiff(self,veh_traj):
        # if hasattr(self,'db'):
        dbDoc = self.toDbDoc(veh_traj,'vehicle')
        # self.db.save(dbDoc)
        response = self.postData(dbDoc)
        print 'vehicle: ' + response

    def writeToFile(self,ped_traj_vec):
        for ped_traj in ped_traj_vec.ped_traj_vec:
            ped_id = ped_traj.ped_id
            with open(self.file_name, 'a') as f:
                for poseStamped in ped_traj.traj:
                    easting, northing = self.xy_to_utm([poseStamped.pose.x], [poseStamped.pose.y])
                    f.write(str([poseStamped.header.stamp.to_sec(), ped_id, poseStamped.pose.x, poseStamped.pose.y, easting[0], northing[0] ]).strip('[]')+'\n')

    def writeToDatabase(self,ped_traj_vec):
        # if hasattr(self, 'db'):
        for ped_traj in ped_traj_vec.ped_traj_vec:
            dbDoc = self.toDbDoc(ped_traj)
            # self.db.save(dbDoc)
            # self.db.update_doc('updater/handle_data', docid=ped_uuid, dbDoc)
            response = self.postData(dbDoc)
            print 'pedestrian: ' +  response

    def postData(self,dbDoc):
        buffer = StringIO()
        c = pycurl.Curl()
        c.setopt(pycurl.URL, '%s' % self.url+ dbDoc['_id'] )
        c.setopt(pycurl.HTTPHEADER, ['Accept: application/json', 'Content-Type: application/json'])
        c.setopt(pycurl.POST, 1)
        c.setopt(pycurl.POSTFIELDS, json.dumps(dbDoc))
        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        c.close()
        return buffer.getvalue()

    def xy_to_utm(self,x,y):
        xy = np.array([x,y,np.ones(len(x))])
        en = np.dot(self.grid_to_utm.transpose(),xy)
        easting = np.divide(en[0],en[2])
        northing = np.divide(en[1],en[2])
        return list(easting), list(northing)

                
if __name__ == '__main__':
    rospy.init_node('ped_logger',anonymous=False)
    rospy.sleep(0.1)
    run_id = rospy.Time.now().secs
    ped_logger = PedLogger(run_id)
    rospy.spin()