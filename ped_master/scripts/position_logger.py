#!/usr/bin/env python
import rospy
import os
import numpy as np
import tf
from couchdb import Server
from datetime import date

from geometry_msgs.msg import PoseStamped

class PositionLogger(object):
    def __init__(self):
        # Setup server
        db_server = rospy.get_param('~db_server','http://swarm:mobility@veh10.mit.edu:5984') #Should default be localhost?
        server = Server(db_server)

        # Setup database
        db_name = rospy.get_param('~db_name','')
        if not db_name:
            db_name = date.today().strftime("app_%m-%d-%Y")
        if db_name in server:
            self.db = server[db_name]
        else:
            self.db = server.create(db_name)        

        #Get vehicle id for doc name
        self.doc_id = os.environ.get('VEHICLE_ID') #Will be empty if not set.
        if not self.doc_id:
            rospy.logwarn("[PositionLogger] VEHICLE_ID not set, using 'test'")
            self.doc_id = 'test'
        rospy.loginfo("[PositionLogger] Logging to " + db_name + '/' + self.doc_id)

        # Get latest document revision so updates can be made
        self.doc_rev = None
        if self.doc_id in self.db:
            doc = self.db[self.doc_id]
            self.doc_rev = doc.rev

        # Get Grid to UTC transform
        map_transform_data = rospy.get_param("~map_transform_data",'/home/swarm/ford_ws/src/ford/ford_ros/maps/stata_aug_2015.py')
        self.grid_to_utm = np.asarray(eval(open(map_transform_data).read()))
        self.grid_to_utm_theta = np.arccos(self.grid_to_utm[0,0]/np.linalg.norm(self.grid_to_utm[0,0:2],2))
        self.map_name = os.path.basename(map_transform_data).strip('.py')

        #subscriber
        self.sub_ped_diff = rospy.Subscriber("~veh_pose",PoseStamped, self.cbPose)


    def cbPose(self,veh_pose):
        x = veh_pose.pose.position.x
        y = veh_pose.pose.position.y
        quaternion = (veh_pose.pose.orientation.x, veh_pose.pose.orientation.y, veh_pose.pose.orientation.z, veh_pose.pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)
        theta = euler[2]

        easting, northing, utm_theta = self.xy_to_utm(x,y,theta)

        dbDoc ={
        '_id':self.doc_id,
        'easting_list':easting,
        'northing_list':northing,
        'utm_theta_list':utm_theta,
        'publish_time':[rospy.Time.now().secs,rospy.Time.now().nsecs]
        }

        if self.doc_rev:
            dbDoc['_rev'] = self.doc_rev

        try:
            doc_id, self.doc_rev = self.db.save(dbDoc)
        except:
            doc = self.db[self.doc_id]
            self.doc_rev = doc.rev

    def xy_to_utm(self,x,y,theta):
        xy = np.array([x,y,1])
        en = np.dot(self.grid_to_utm.transpose(),xy)
        easting = np.divide(en[0],en[2])
        northing = np.divide(en[1],en[2])
        utm_theta = theta - self.grid_to_utm_theta
        return easting, northing, utm_theta

                
if __name__ == '__main__':
    rospy.init_node('positoin_logger',anonymous=False)
    rospy.sleep(0.1)
    db_logger = PositionLogger()
    rospy.spin()