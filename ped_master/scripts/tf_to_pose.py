#!/usr/bin/env python
import rospy
import tf
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped

class ConverterNode(object):
    def __init__(self):
        self.source_frame = rospy.get_param("~source_frame","source_frame")
        self.target_frame = rospy.get_param("~target_frame","target_frame")
        self.rate = float(rospy.get_param("~rate",50))
        
        self.pub = rospy.Publisher("~pose_out",PoseStamped, queue_size=1)
        self.pub_cov = rospy.Publisher("~pose_out_cov",PoseWithCovarianceStamped, queue_size=1)
        self.listener = tf.TransformListener()
        self.timer = rospy.Timer(rospy.Duration.from_sec(1/self.rate),self.cbTimer)

    def cbTimer(self,event):
        try:
            (trans,rot) = self.listener.lookupTransform(self.target_frame, self.source_frame, rospy.Time(0))
            msg_out = PoseStamped()
            msg_out.pose.position.x = trans[0]
            msg_out.pose.position.y = trans[1]
            msg_out.pose.position.z = trans[2]
            msg_out.pose.orientation.x = rot[0]
            msg_out.pose.orientation.y = rot[1]
            msg_out.pose.orientation.z = rot[2]
            msg_out.pose.orientation.w = rot[3]
            msg_out.header.stamp = rospy.Time.now()
            msg_out.header.frame_id = self.target_frame
            self.pub.publish(msg_out)

            msg_out_cov = PoseWithCovarianceStamped()
            msg_out_cov.header = msg_out.header
            msg_out_cov.pose.pose = msg_out.pose
            self.pub_cov.publish(msg_out_cov)

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            # rospy.logwarning("Not getting tf.")
            pass

if __name__ == '__main__':
    rospy.init_node("tf_to_pose")
    node = ConverterNode()
    rospy.spin()