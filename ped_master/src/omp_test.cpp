#include <ros/ros.h>
#include <omp.h>
#include <sensor_msgs/LaserScan.h>
#include <math.h>


class OmpTest{
public:
  ros::NodeHandle nh_p_;
  // sensor_msgs::LaserScan laserScan;

  OmpTest()
  {
    nh_p_ = ros::NodeHandle("~");
    /* Populate LaserScan */
    ROS_INFO_STREAM("[OmpTest] Without Omp: " << test(false));
    // ROS_INFO_STREAM("[OmpTest] Without Omp: " << test(false));
    // ROS_INFO_STREAM("[OmpTest] Without Omp: " << test(false));
    // ROS_INFO_STREAM("[OmpTest] Without Omp: " << test(false));
    // ROS_INFO_STREAM("[OmpTest] Without Omp: " << test(false));
    #ifdef _OPENMP
    ROS_INFO_STREAM("[OmpTest] With Omp: " << test(true));
    // ROS_INFO_STREAM("[OmpTest] With Omp: " << test(true));
    // ROS_INFO_STREAM("[OmpTest] With Omp: " << test(true));
    // ROS_INFO_STREAM("[OmpTest] With Omp: " << test(true));
    // ROS_INFO_STREAM("[OmpTest] With Omp: " << test(true));
    #endif

    // print_test(false);
    // print_test(true);

  }
  ~OmpTest(){}

  double test(bool use_omp)
  {
    std::vector<double> v(100000000);
    ros::Time start_time = ros::Time::now();

    // int v_size = v.size();
    if (use_omp){
      #pragma omp parallel for
      for (int i = 0; i < v.size(); ++i){
        double& d = v[i];
        d = cos((double) i);
      }      
    }
    else{
      for (int i = 0; i < v.size(); ++i){
        double& d = v[i];
        d = cos((double) i);
      }            
    }
    return (ros::Time::now() - start_time).toSec();
  }

  void print_test(bool use_omp)
  {
    ros::Time start_time = ros::Time::now();
    if (use_omp){
      #pragma omp parallel for num_threads(4)
      for (int i = 0; i < 10; ++i){
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
    else{
      for (int i = 0; i < 10; ++i){
        std::cout << i << " ";
      }            
      std::cout << std::endl;
    }
    ROS_INFO_STREAM("Took:" << (ros::Time::now() - start_time).toSec());
  }

};

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "omp_test");
    OmpTest omp_test;
    ros::spin();
    return 0;
}