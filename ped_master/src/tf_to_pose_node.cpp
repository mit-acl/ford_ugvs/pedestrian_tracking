#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>

class Tf2PoseNode{
public:
    ros::NodeHandle nh_p_;

    ros::Publisher pub_pose_;
    tf::TransformListener tfListerner_;
    geometry_msgs::PoseStamped pose_;
    ros::Timer timer_pub_;

    std::string source_frame_;
    std::string target_frame_;
    double pub_duration_;

    bool publish_success_;
    
    Tf2PoseNode()
    {
        nh_p_ = ros::NodeHandle("~");
        setDefaultParameters();
        getParameters();

        /* Publisher */
        pub_pose_ = nh_p_.advertise<geometry_msgs::PoseStamped>("output",1);

        /* Initialize unit vector at the source frame */
        pose_.header.stamp = ros::Time::now();
        pose_.header.frame_id = source_frame_;
        pose_.pose.orientation.w = 1.0;

        publish_success_ = false;
        /* Timers */
        timer_pub_ = nh_p_.createTimer(ros::Duration(pub_duration_),&Tf2PoseNode::cbPubTimer,this);
    }
    ~Tf2PoseNode(){}

    void setDefaultParameters()
    {
        if (!ros::param::has("~source_frame")) { ros::param::set("~source_frame","/base_link");}
        if (!ros::param::has("~target_frame")) { ros::param::set("~target_frame","/map");}
        if (!ros::param::has("~pub_duration")) { ros::param::set("~pub_duration",0.01);}
    }
    void getParameters()
    {
        ros::param::getCached("~source_frame",source_frame_);
        ros::param::getCached("~target_frame",target_frame_);
        ros::param::getCached("~pub_duration",pub_duration_);
        ROS_INFO_STREAM("[Tf2PoseNode] source_frame_:" << source_frame_);
        ROS_INFO_STREAM("[Tf2PoseNode] target_frame_:" << target_frame_);
        ROS_INFO_STREAM("[Tf2PoseNode] pub_duration_:" << pub_duration_);
    }
    void cbPubTimer(const ros::TimerEvent& timerEvent)
    {
        // ROS_INFO_STREAM("[Tf2PoseNode] Called.");
        if (publish_success_){
            /* Last cb was a success, update timestamp with current time.*/
            pose_.header.stamp = ros::Time::now();
        }

        if(tfListerner_.waitForTransform(target_frame_,pose_.header.frame_id,
            pose_.header.stamp,ros::Duration(pub_duration_))){
            /* Transform availabe */
            geometry_msgs::PoseStamped pose_out;
            try{
                tfListerner_.transformPose(target_frame_,pose_,pose_out);
            }
            catch (tf::TransformException ex){
                /*tf still not available, shouldn't happen.*/
                ROS_ERROR("%s",ex.what());
                return;
            }
            pub_pose_.publish(pose_out);
            publish_success_ = true;
            // ROS_INFO("[Tf2PoseNode] Published: %f",pose_out.header.stamp.toSec());
        }
        else{
            /* Timed out. Don't publish*/
            publish_success_ = false;
            // ros::Time now = ros::Time::now();
            // ROS_ERROR_STREAM("[Tf2PoseNode] Stamp: " << pose_.header.stamp.toSec());
            // ROS_ERROR_STREAM("[Tf2PoseNode] Now  : " << now.toSec());
            // ROS_ERROR_STREAM("[Tf2PoseNode] Diff  : " << (now - pose_.header.stamp).toSec());
            // ROS_ERROR("[Tf2PoseNode] waitForTransform timed out.");
            return;
        }
    }
};

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "tf_to_pose_node");
    Tf2PoseNode node;
    ros::spin();
    return 0;
}