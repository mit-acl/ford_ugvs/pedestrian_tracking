#ifndef __PCL_CLUSTERING_UTILPCL_HPP


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/PointIndices.h>
#include <pcl/search/search.h>
#include <vector>

namespace utilpcl
{
  void test_function();
  pcl::PointCloud<pcl::PointXYZ>::Ptr downsample(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, double leafSize);

  pcl::PointIndices::Ptr get_planar_indicies_ransac(
    const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, int max_iteration, double distance_threshold,
    pcl::ModelCoefficients::Ptr model_coefficients);

  pcl::PointCloud<pcl::PointXYZ>::Ptr extract_pointcloud_by_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, pcl::PointIndices::Ptr indices, bool negative = false);

  std::vector<pcl::PointIndices> get_euclidean_cluster_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, double cluster_tolerance, int min_cluster_size, int max_cluster_size);

  pcl::PointCloud<pcl::PointXYZ>::Ptr project_pointcloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, pcl::ModelCoefficients::Ptr model_coefficients);

  pcl::PointCloud<pcl::Normal>::Ptr get_normals(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, double search_radious, pcl::search::Search<pcl::PointXYZ>::Ptr tree_out);

  std::vector<pcl::PointIndices> get_region_growing_cluster_indicies(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud,
    int min_cluster_size, int max_cluster_size, int num_of_neighbors, double search_radious, 
    double smooth_threshold, double curvature_threshold);

  pcl::PointIndices::Ptr get_passthrough_indicies(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, std::string filter_field, double value_min, double value_max);

  pcl::PointIndices::Ptr get_plane_inlier_indicies(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, pcl::ModelCoefficients::Ptr model_coefficients, double distance_threshold);

  pcl::PointCloud<pcl::PointXYZ>::Ptr remove_radius_outlier(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, double radius, int min_neighbors);

  pcl::PointCloud<pcl::PointXYZ>::Ptr get_concave_hull(const pcl::PointCloud<pcl::PointXYZ>::Ptr proj_cloud, double alpha);

  pcl::PointIndices::Ptr get_wall_indicies_ransac(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, int max_iteration, double distance_threshold, pcl::ModelCoefficients::Ptr model_coefficients);

}

#include "utilpcl_impl.hpp"
#define __PCL_CLUSTERING_UTILPCL_HPP
#endif /* __PCL_CLUSTERING_UTILPCL_HPP */