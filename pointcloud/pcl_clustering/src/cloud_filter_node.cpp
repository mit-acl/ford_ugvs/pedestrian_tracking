#include <vector>
#include <stdlib.h> 

#include <ros/ros.h>
#include <std_srvs/Empty.h>

// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/octree/octree.h>

#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>

#include <utilpcl.hpp>


class CloudFilter{
public:

	ros::NodeHandle nh;
	ros::Publisher pub;
	ros::Subscriber sub;
	ros::ServiceServer srv;
	
	tf::TransformListener* tfListener;
	
	pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>* octree;

	bool has_key_cloud;
	pcl::PointCloud<pcl::PointXYZ>::Ptr key_cloud;

	pcl::ModelCoefficients::Ptr ground_coefficient;

	CloudFilter(int argc, char** argv, double octree_res)
	{
		// Setup node handler
		nh = ros::NodeHandle("~");
		pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
		sub = nh.subscribe("input", 5, &CloudFilter::class_callback, this);
		srv = nh.advertiseService("reset_key_cloud", &CloudFilter::srv_reset_key_cloud ,this);
		tfListener = new tf::TransformListener(ros::Duration(60));
		// Initialize octree
		octree = new pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>(octree_res);
		has_key_cloud = false;

		// ===Default parameters===

		// Set goal frame
	    if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world");}
	    
	    if (!ros::param::has("~passthough/flag")) { ros::param::set("~passthough/flag",true);}

		// Diff filter
		if (!ros::param::has("~diff_filter/flag")) { ros::param::set("~diff_filter/flag",true);}

		// Noise removal
	    if (!ros::param::has("~noise_remove/flag")) { ros::param::set("~noise_remove/flag",true);}
	    if (!ros::param::has("~noise_remove/radius")) { ros::param::set("~noise_remove/radius",0.1);}
	    if (!ros::param::has("~noise_remove/min_neighbor")) { ros::param::set("~noise_remove/min_neighbor",5);}

	    // Downsample
	    if (!ros::param::has("~downsample/flag")) { ros::param::set("~downsample/flag",true);}
	    if (!ros::param::has("~downsample/leaf_size")) { ros::param::set("~downsample/leaf_size",0.2);}

	    // Ground removal
	    if (!ros::param::has("~ground_remove/flag")) { ros::param::set("~ground_remove/flag",true); };
	    if (!ros::param::has("~ground_remove/max_iteration")) { ros::param::set("~ground_remove/max_iteration",500); };
	    if (!ros::param::has("~ground_remove/distance_threshold")) { ros::param::set("~ground_remove/distance_threshold",0.1); };
	    if (!ros::param::has("~ground_remove/use_fixed_ground")) { ros::param::set("~ground_remove/use_fixed_ground",true); };
	    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients());
	    ground_coefficient = coefficient;
	    ground_coefficient->values.push_back(-0.00627247);
	    ground_coefficient->values.push_back(-0.00825334);
	    ground_coefficient->values.push_back(0.999946);
	    ground_coefficient->values.push_back(-0.0295839);



	}

	bool srv_reset_key_cloud(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
	{
		ROS_INFO_STREAM("srv_reset_key_cloud called");
		has_key_cloud = false;

		return true;
	}

	void set_key_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
	{	
		key_cloud = cloud;
		has_key_cloud = true;
		ROS_INFO_STREAM("Key cloud set.");
	}

	void load_key_cloud(){
		// Load the key cloud into the octree
		octree->deleteCurrentBuffer();
		octree->switchBuffers();
		octree->setInputCloud(key_cloud);
		octree->addPointsFromInputCloud();
		octree->switchBuffers();
		ROS_INFO_STREAM("Key cloud loaded.");
	}

	int get_diff_to_key(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out)
	{
		// Return the number of points in cloud_in that are not in the key cloud.
		// Put them in cloud_out
		octree->setInputCloud (cloud_in);
		octree->addPointsFromInputCloud ();
		// Get vector of point indices from octree voxels which did not exist in previous buffer
		std::vector<int> newPointIdxVector;
		octree->getPointIndicesFromNewVoxels (newPointIdxVector);

		// Use extract indices to extract the differ pointcloud
		pcl::PointIndices::Ptr diffIndices (new pcl::PointIndices ());
		diffIndices->indices = newPointIdxVector;

		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud (cloud_in);
		extract.setIndices (diffIndices);
		extract.setNegative (false);
		extract.filter (*cloud_out);

		// Remove cloud_in from current buffer
		octree->deleteCurrentBuffer();

		return newPointIdxVector.size();
	}

	void class_callback(const sensor_msgs::PointCloud2& input)
	{

		// ===Convert the transformed cloud to pcl::PointCloud format===
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_input (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromROSMsg (input, *cloud_input);

    /* Remove NaNs */
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
    std::vector<int> temp_ind;
    pcl::removeNaNFromPointCloud(*cloud_input,*cloud,temp_ind);

		bool passthrough_flag = true; ros::param::getCached("~passthough/flag",passthrough_flag);
		if (passthrough_flag){
		    pcl::PointIndices::Ptr passIndices = utilpcl::get_passthrough_indicies(cloud,"y",0.0,50.0);
			cloud = utilpcl::extract_pointcloud_by_indices(cloud,passIndices);
		}

		// ===Diff filter===
		bool diff_filter_flag; ros::param::getCached("~diff_filter/flag",diff_filter_flag);
		if (diff_filter_flag){
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_temp(new pcl::PointCloud<pcl::PointXYZ>);
			if (get_cloud_diff(cloud, cloud_temp)){
				// Replace cloud with the diff-filtered cloud
				// ROS_INFO_STREAM("size of cloud_temp in callback " << cloud_temp->size() );
				// ROS_INFO_STREAM("Cloud size before temp assignment." << cloud->size() );
				cloud = cloud_temp;
				
				if(cloud->size()==0){ROS_INFO_STREAM("Empty cloud after diff filter.");return;}
			}
			else{
				// Key Frame reloaded. Skip this frame.
				// ROS_INFO_STREAM("get_cloud_diff returned false.");
				return;
			}
		}

		// ===Transform frame=== For some reason the cloud_diff doesn't like the transformed data.
		// ROS_INFO_STREAM("Frame id of cloud before tf: " << cloud->header.frame_id);
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tf(new pcl::PointCloud<pcl::PointXYZ>);
    std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);
    tfListener->waitForTransform(goal_frame, input.header.frame_id, ros::Time(0), ros::Duration(5.0));
    pcl_ros::transformPointCloud(goal_frame,*cloud, *cloud_tf, *tfListener);
    cloud = cloud_tf;
    // ROS_INFO_STREAM("Frame id of cloud after tf: " << cloud->header.frame_id);


		// ===Downsample===
		double leaf_size; ros::param::getCached("~downsample/leaf_size",leaf_size);
    bool downsample_flag = true; ros::param::getCached("~downsample/flag",downsample_flag);
    if (downsample_flag){
      cloud = utilpcl::downsample(cloud,leaf_size);
      if(cloud->size()==0){ROS_INFO_STREAM("Empty cloud after down sample.");return;}
    }
    // ===Noise Removal===
    bool noise_remove_flag; ros::param::getCached("~noise_remove/flag",noise_remove_flag);
    if (noise_remove_flag){
      // Read parameters
      double noise_remove_radius; ros::param::getCached("~noise_remove/radius",noise_remove_radius);
      int noise_remove_min_neighbor; ros::param::getCached("~noise_remove/min_neighbor",noise_remove_min_neighbor);
      cloud = utilpcl::remove_radius_outlier(cloud, noise_remove_radius, noise_remove_min_neighbor);
      if(cloud->size()==0){ROS_INFO_STREAM("Empty cloud after noise removal.");return;}
    }


    // ===Remove ground===
    bool ground_remove_flag; ros::param::getCached("~ground_remove/flag",ground_remove_flag);
    bool use_fixed_ground; ros::param::getCached("~ground_remove/use_fixed_ground",use_fixed_ground);
    double distance_threshold; ros::param::getCached("~ground_remove/distance_threshold",distance_threshold);
    pcl::PointIndices::Ptr ground_indices;
    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients());
    ground_coefficient->header = cloud->header;

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ground_hull (new pcl::PointCloud<pcl::PointXYZ>);


    if (ground_remove_flag){
      if (use_fixed_ground){
        // Use the preset ground coefficient 
        coefficient = ground_coefficient;
        ground_indices = utilpcl::get_plane_inlier_indicies(cloud,ground_coefficient,distance_threshold);
      } 
      else{
        // Use ransac to get the coefficient of the ground
        int max_iteration; ros::param::getCached("~ground_remove/max_iteration",max_iteration);
        ground_indices = utilpcl::get_planar_indicies_ransac(cloud, max_iteration, distance_threshold, coefficient);

        // ROS_INFO_STREAM("Ground Coeff values:" << coefficient->values[0] << " " << coefficient->values[1] << " " << coefficient->values[2] << " "<< coefficient->values[3] << " " );
        // ROS_INFO_STREAM("Ground Coeff header:" << coefficient->header);
        if (coefficient->values.size() < 4){
          ROS_INFO_STREAM("coefficient extraction failed. Using preset coefficient.");
          coefficient = ground_coefficient;
          ground_indices = utilpcl::get_plane_inlier_indicies(cloud,coefficient,distance_threshold);
        }
        ros::param::set("ground_coefficient",coefficient->values);
      }
      // Remove the ground



      // cloud_ground_hull = utilpcl::extract_pointcloud_by_indices(cloud, ground_indices, false);
      cloud = utilpcl::extract_pointcloud_by_indices(cloud, ground_indices, true);
      
      // int max_iteration; ros::param::getCached("~ground_remove/max_iteration",max_iteration);

	  // pcl::PointIndices::Ptr wall_indicies(new pcl::PointIndices());
	  // wall_indicies = utilpcl::get_wall_indicies_ransac(cloud, max_iteration, distance_threshold, coefficient);
	  // ROS_INFO_STREAM("wall_indicies.size:" << wall_indicies->indices.size());
      // ROS_INFO_STREAM("wall coefficient:" << coefficient->values);
      // ROS_INFO_STREAM("wall coefficient:" << coefficient->values[0] << " " << coefficient->values[1] << " " << coefficient->values[2] << " "<< coefficient->values[3] << " " );


      // cloud = utilpcl::extract_pointcloud_by_indices(cloud, wall_indicies, false);


      // cloud_ground_hull = utilpcl::get_concave_hull(cloud_ground_hull,2.0);

	  // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_proj (new pcl::PointCloud<pcl::PointXYZ>);
      // cloud_proj = utilpcl::project_pointcloud(cloud, coefficient);

  //     pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
  //     kdtree.setInputCloud (cloud_ground_hull);

	 //  pcl::PointIndices::Ptr keep_indicies(new pcl::PointIndices());

	 //  int index = 0;
  // 	  int K = 1;
  //     for( pcl::PointCloud<pcl::PointXYZ>::iterator iter = cloud_proj->begin(); iter != cloud_proj->end(); ++iter)
  //     {
		// std::vector<int> pointIdxNKNSearch(K);
		// std::vector<float> pointNKNSquaredDistance(K);
		// if ( kdtree.nearestKSearch(*iter, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
		// {
		// 	if (pointNKNSquaredDistance[0] > 0.3)
		// 	{
		//       	keep_indicies->indices.push_back(index);
		// 	}
		// }
  //     	index++;
  //     }
  //     // cloud = utilpcl::extract_pointcloud_by_indices(cloud, keep_indicies, false);
  //     cloud = cloud_ground_hull;

  //     ROS_INFO_STREAM("cloud_proj size:" << cloud_proj->size());
  //     ROS_INFO_STREAM("index:" << index);
  //     ROS_INFO_STREAM("keep_indicies:" << keep_indicies->indices.size());

      if(cloud->size()==0){ROS_INFO_STREAM("Empty cloud after ground remove.");return;}
    }

    // // Project the pointcloud on the floor
    // bool project_to_ground; ros::param::getCached("~project_to_ground",project_to_ground);
    // if (project_to_ground){
    //   cloud = utilpcl::project_pointcloud(cloud, coefficient);      
    // }

	// pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
	// prism.setInputCloud (cloud);
	// prism.setInputPlanarHull (cloud_ground);
	// prism.setHeightLimits (0.1,3.0);
	// pcl::PointIndices::Ptr prism_indices(new pcl::PointIndices());
	// prism.segment (*prism_indices);
	// cloud = utilpcl::extract_pointcloud_by_indices(cloud, prism_indices, true);






		// ===Convert to ROSMsg===
		sensor_msgs::PointCloud2 cloud_pub;
		pcl::toROSMsg(*cloud, cloud_pub);
		// pcl::toROSMsg(*cloud_ground, cloud_pub);
		// Stamp and frame (to goal frame)
		
		cloud_pub.header.frame_id = goal_frame;
		cloud_pub.header.stamp = input.header.stamp; //TODO see if this is appropreate
		
		// Publish
		pub.publish(cloud_pub);
	}

	bool get_cloud_diff(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff_out)
	{
		if (!has_key_cloud){
			// No key cloud yet then set current input as key cloud
			set_key_cloud(cloud);
			// Load it to buffer
			load_key_cloud();
			return false;
		}
		else{
			
			int size_of_diff = get_diff_to_key(cloud,cloud_diff_out);

			// ROS_INFO_STREAM("Size of cloud: " << cloud->size());
			
			double change_ratio = (double)size_of_diff/(double) cloud->size();

			if (change_ratio < 0.99){
				// Acceptable change rate.
		    // ROS_INFO_STREAM("cloud_diff_out size:" << cloud_diff_out->size());
		    return true;
			}
			else{
				// Re-load the key cloud when change rate is too high
				load_key_cloud();
				// Don't return cloud_diff_out
				return false;
			}
		}
	}

};


int main (int argc, char**argv)
{
	ros::init (argc, argv, "oct_background_remove");

	// Set default octree resolution
	if (!ros::param::has("~diff_filter/octree_res")) { ros::param::set("~diff_filter/octree_res",0.2);}

	// Initializet the filter obj
	double octree_res; ros::param::getCached("~diff_filter/octree_res",octree_res);
	CloudFilter cloudFliterObj(argc, argv, octree_res);


	ros::spin();
	return 0;
}