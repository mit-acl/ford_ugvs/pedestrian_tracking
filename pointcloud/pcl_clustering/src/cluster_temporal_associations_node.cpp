#include <ros/ros.h>
#include <algorithm>
#include <vector>

// Msg/srv
#include <ford_msgs/Clusters.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

// For ROS to interact with PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/common.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/search/pcl_search.h>

class TemporalAssociationsClass{
public:
  ros::NodeHandle nh;
  ros::Publisher pub_marker;
  ros::Publisher pub_clusters;
  ros::Subscriber sub;

  struct rgb{
    double r;
    double g;
    double b;
  };

  std::vector<rgb> colormap;
  
  int max_marker_id_;

  pcl::search::Search<pcl::PointXYZ>* prevKd;
  ford_msgs::Clusters prevClusts;

  double dist_thresh;

  TemporalAssociationsClass(int argc, char** argv)
  {

    max_marker_id_ = 10000;
    // Setup node handler
    nh = ros::NodeHandle("~");

    pub_marker = nh.advertise<visualization_msgs::MarkerArray> ("output/marker_array",1);
    pub_clusters = nh.advertise<ford_msgs::Clusters> ("output/clusters",1);

    sub = nh.subscribe("input", 1, &TemporalAssociationsClass::class_callback, this);

    nh.param("dist_thresh", dist_thresh, 0.01);

  }

  void class_callback(const ford_msgs::Clusters& input)
  {
    ford_msgs::Clusters assocOutput = input;

    //ROS_INFO_STREAM("[CLUSTER TEMPORAL ASSOCIATION CALLBACK] entered callback");
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr clean_cloud (new pcl::PointCloud<pcl::PointXYZ>);

    cloud->points.resize(input.mean_points.size());

    // add all mean points to a new cloud
    for (int i = 0; i < input.mean_points.size(); i++) {
      cloud->points[i].x = input.mean_points[i].x;
      cloud->points[i].y = input.mean_points[i].y;
      cloud->points[i].z = input.mean_points[i].z;
    }

    // clean cloud of NaNs
    //pcl::removeNaNFromPointCloud(*cloud, *clean_cloud, temp_ind);

    // if there is no previous cloud, set it to the current cloud and exit
    if (!prevKd) {
      ROS_INFO_STREAM("[CLUSTER TEMPORAL ASSOCIATION CALLBACK] setting up initial cloud");
      prevKd = new pcl::search::KdTree<pcl::PointXYZ> ();
      //prevKd->setInputCloud(clean_cloud);
      prevKd->setInputCloud(cloud);
      prevClusts = input;
      return;
    }
    

    int K = 1;
    pcl::PointXYZ searchPoint;
    std::vector<int> NNIdx(K);
    std::vector<float> NNSquaredDist(K);

    for (int i = 0; i < input.mean_points.size(); i++) {
      searchPoint.x = input.mean_points[i].x;
      searchPoint.y = input.mean_points[i].y;
      searchPoint.z = input.mean_points[i].z;

      prevKd->nearestKSearch(searchPoint, K, NNIdx, NNSquaredDist);

      if (NNSquaredDist[0] < dist_thresh) {
        assocOutput.labels[i] = prevClusts.labels[ NNIdx[0] ];
        //ROS_INFO_STREAM("nearest neighbor: (" << clean_cloud->points[ NNIdx[0] ].x << ", " << clean_cloud->points[ NNIdx[0] ].y << ", " << clean_cloud->points[ NNIdx[0] ].z << "), dist: " << NNSquaredDist[0]);
        //ROS_INFO_STREAM("nearest neighbor: " << cloud->points[ NNIdx[0] ].x);
      }
    }

    //prevKd->setInputCloud(clean_cloud);
    prevKd->setInputCloud(cloud);
    prevClusts = assocOutput;

    pub_clusters.publish(assocOutput);

    gen_marker_from_message(assocOutput);

    return;
  }

  void gen_marker_from_message(ford_msgs::Clusters& clusters){
    visualization_msgs::MarkerArray markers;
    std::vector<visualization_msgs::Marker> markerVector;

    for(int i = 0; i < clusters.labels.size(); i++){
      visualization_msgs::Marker marker;

      marker.header.frame_id = clusters.header.frame_id;
      marker.header.stamp = ros::Time::now();
      marker.ns = "text";
      marker.id = clusters.labels[i] % max_marker_id_;

      marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
      marker.action = visualization_msgs::Marker::ADD;

      marker.pose.position.x = clusters.mean_points[i].x;
      marker.pose.position.y = clusters.mean_points[i].y;
      marker.pose.position.z = clusters.mean_points[i].z;

      marker.scale.x = 0.5;
      marker.scale.y = 0.5;
      marker.scale.z = 0.5;

      marker.color.a = 1.0;
      marker.color.r = 0.0;
      marker.color.g = 1.0;
      marker.color.b = 0.0;

      marker.lifetime = ros::Duration(0.5);

      std::stringstream ss;
      ss << "assoc id: " << clusters.labels[i] << std::endl;
      marker.text = ss.str();

      markerVector.push_back( marker );

    }
    markers.markers.insert(markers.markers.end(), markerVector.begin(), markerVector.end());

    pub_marker.publish(markers);
  }

};




int main (int argc, char**argv)
{
  ros::init (argc, argv, "cluster_temporal_associations_node");
  TemporalAssociationsClass temporalAssociationsClassObj(argc, argv);

  ros::spin();
  return 0;
}
