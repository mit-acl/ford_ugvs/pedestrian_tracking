#include <ros/ros.h>

#include <stdlib.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/common/common.h>

#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>

#include <utilpcl.hpp>


typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, sensor_msgs::LaserScan> MySyncPolicy;
class DoubleLidarToCloud{
public:

  ros::NodeHandle nh;
  message_filters::Subscriber<sensor_msgs::LaserScan> image1_sub;
  message_filters::Subscriber<sensor_msgs::LaserScan> image2_sub;
  message_filters::Synchronizer<MySyncPolicy>* sync;
  ros::Publisher pub_cloud;
  
  // ros::Publisher pub_marker;
  tf::TransformListener* tfListener;
  laser_geometry::LaserProjection projector;

  // std::vector<rgb> colormap;

  DoubleLidarToCloud(int argc, char** argv)
  {
    nh = ros::NodeHandle("~");
    image1_sub.subscribe(nh,"input_1",2);
    image2_sub.subscribe(nh,"input_2",2);
    sync = new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(10), image1_sub, image2_sub);
    sync->registerCallback(boost::bind(&DoubleLidarToCloud::callback,this, _1, _2));
    tfListener = new tf::TransformListener(ros::Duration(60));
 
    // Advertise Publishment
    pub_cloud = nh.advertise<sensor_msgs::PointCloud2> ("output",5);

    // Set default parameters
    if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world");}

  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr laserScanToPointCloud(const sensor_msgs::LaserScan::ConstPtr& laserScan, std::string goal_frame)
  {
    // Convert LaserScan msg to pointcloud. Transform into the goal_frame specified
    sensor_msgs::PointCloud2::Ptr lidar_msg(new sensor_msgs::PointCloud2);

    tfListener->waitForTransform(goal_frame, laserScan->header.frame_id, ros::Time(0), ros::Duration(10.0));
    projector.projectLaser(*laserScan, *lidar_msg);

    sensor_msgs::PointCloud2::Ptr lidar_msg_tf(new sensor_msgs::PointCloud2);
    pcl_ros::transformPointCloud(goal_frame,*lidar_msg,*lidar_msg_tf,*tfListener);

    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_pointCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (*lidar_msg_tf,*lidar_pointCloud);

    return lidar_pointCloud;
  }

  void callback(const sensor_msgs::LaserScan::ConstPtr& input_top, const sensor_msgs::LaserScan::ConstPtr& input_bot)
  {
    
    // ROS_INFO_STREAM("Class Sync Called!!!");
    std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_top = laserScanToPointCloud(input_top,goal_frame);
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_bot = laserScanToPointCloud(input_bot,goal_frame);
    // ROS_INFO_STREAM("lidar_top.size()" << lidar_top->size());
    // ROS_INFO_STREAM("lidar_bot.size()" << lidar_bot->size());

    // Combine them into the same pointcloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar(new pcl::PointCloud<pcl::PointXYZ>);
    *lidar = *lidar_top + *lidar_bot;
    sensor_msgs::PointCloud2::Ptr cloud_pub(new sensor_msgs::PointCloud2);
    pcl::toROSMsg(*lidar, *cloud_pub);
    cloud_pub->header.frame_id = goal_frame; 
    cloud_pub->header.stamp = ros::Time::now();
    pub_cloud.publish(cloud_pub);
  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "lidar_to_cloud");
  DoubleLidarToCloud lidarObj(argc,argv);
  ros::spin();
  return 0;
}