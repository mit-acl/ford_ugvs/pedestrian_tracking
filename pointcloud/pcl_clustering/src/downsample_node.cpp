#include <ros/ros.h>

// For ROS to interact with PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

// For using utilpcl
#include <utilpcl.h>

class DownSample{
public:
  ros::NodeHandle nh;
  ros::Publisher pub;
  ros::Subscriber sub;
  
  DownSample(int argc, char** argv)
  {
    // Setup node handler
    nh = ros::NodeHandle("~");
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
    sub = nh.subscribe("input", 5, &DownSample::class_callback, this);

    // Read parameters (or set default)
    if (!ros::param::has("~leaf_size")) { ros::param::set("~leaf_size",0.1);}

    // srv = nh.advertiseService("reset_key_cloud", &DownSample::srv_reset_key_cloud ,this);
    // Initialize octree
  }

  void class_callback(const sensor_msgs::PointCloud2& input)
  {
    // Convert input to pcl::PointCloud format
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (input, *cloud);

    // Check parameter
    double leafSize; ros::param::getCached("~leaf_size",leafSize);

    // Call the downsample funciton
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered = utilpcl::downsample(cloud,leafSize);

    // Convert and publish the filtered point cloud
    sensor_msgs::PointCloud2 cloud_pub;
    pcl::toROSMsg(*cloud_filtered, cloud_pub);
    // Put the processed cloud under the same frame as the input
    cloud_pub.header.frame_id = input.header.frame_id; 
    cloud_pub.header.stamp = input.header.stamp;
    pub.publish(cloud_pub);
  }

};


int main (int argc, char**argv)
{
  ros::init (argc, argv, "downsample");
  DownSample downSample(argc, argv);
  ros::spin();
  return 0;
}