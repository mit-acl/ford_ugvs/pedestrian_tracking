// Modified from the code posted on http://www.pcl-users.org/Using-GroundBasedPeopleDetectionApp-with-ROS-memory-leak-td4026867.html by Alexey Ozhigov

// #include <pcl/console/parse.h>
// #include <pcl/io/openni_grabber.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl/people/ground_based_people_detection_app.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>



// #include <pcl/point_cloud.h>
// #include <pcl/point_types.h>
// #include <pcl/ModelCoefficients.h>


#define POINT_CLOUD_TOPIC_DEF "/camera/depth_registered/points"
#define MARKERARRAY_TOPIC_DEF "output_markerarray"

using namespace std;

typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

int print_help(){
    cout << "*******************************************************" << endl;
    cout << "Ground based people detection app options:" << endl;
    cout << "   --help    <show_this_help>" << endl;
    cout << "   --svm     <path_to_svm_file>" << endl;
    cout << "   --conf    <minimum_HOG_confidence (default = -1.5)>" << endl;
    cout << "   --min_h   <minimum_person_height (default = 1.3)>" << endl;
    cout << "   --max_h   <maximum_person_height (default = 2.3)>" << endl;
    cout << "*******************************************************" << endl;
    return 0;
}

class PeopleTracker{
public:
    string svm_filename;
    float min_confidence;
    float min_height;
    float max_height;
    float voxel_size;
    pcl::people::GroundBasedPeopleDetectionApp<PointT> people_detector;
    PointCloudT::Ptr ground_points_3d;
    PointCloudT cloud_obj;
    PointCloudT::Ptr cloud;
    PointCloudT::Ptr person_cloud;

    pcl::people::PersonClassifier<pcl::RGB> person_classifier;
    Eigen::Matrix3f rgb_intrinsics_matrix;
    ros::Subscriber sub_points;
    ros::Publisher pub_marker;
    // ros::ServiceServer srv_setground;
    


    ros::NodeHandle nh;
    Eigen::VectorXf ground_coeffs;

    // pcl::visualization::PCLVisualizer viewer;

    PeopleTracker(int argc, char** argv, string svm_filename_ = "~/svmfile.yaml",
                  float min_confidence_ = - 1.5,
                  float min_height_ = 1.3,
                  float max_height_ = 2.3,
                  float voxel_size_ = 0.06):
            svm_filename(svm_filename_),
            min_height(min_height_),
            max_height(max_height_),
            voxel_size(voxel_size_)
    {
        rgb_intrinsics_matrix << 525, 0.0, 319.5, 0.0, 525, 239.5, 0.0, 0.0, 1.0; // Kinect RGB camera intrinsics
        
        // Create classifier for people detection:  
        string mysvmfilepath = ros::package::getPath("pcl_clustering")+"/data/svmfile.yaml";
        
        ROS_INFO_STREAM("Loading SVM file from: " << mysvmfilepath);
        // std::cout << "SVM is at: " << mysvmfilepath << std::endl;
        

        person_classifier.loadSVMFromFile(mysvmfilepath);   // load trained SVM

        // People detection app initialization:
        people_detector.setVoxelSize(voxel_size);                        // set the voxel size
        people_detector.setIntrinsics(rgb_intrinsics_matrix);            // set RGB camera intrinsic parameters
        people_detector.setClassifier(person_classifier);                // set person classifier
        people_detector.setHeightLimits(min_height, max_height);         // set person classifier
        //  people_detector.setSensorPortraitOrientation(true);             // set sensor orientation to vertical

        ground_points_3d = PointCloudT::Ptr(new PointCloudT);
        cloud = PointCloudT::Ptr(new PointCloudT);
        person_cloud = PointCloudT::Ptr(new PointCloudT);

        sub_points = nh.subscribe(POINT_CLOUD_TOPIC_DEF, 1, &PeopleTracker::cloud_cb, this);
        pub_marker = nh.advertise<visualization_msgs::MarkerArray> (MARKERARRAY_TOPIC_DEF,10);

        // // Initialize viewer
        // viewer = pcl::visualization::PCLVisualizer("PCL Viewer");
        // viewer.setCameraPosition(0,0,-2,0,-1,0,0);
    }

    void cloud_cb (const sensor_msgs::PointCloud2& input){
        // ROS_INFO_STREAM("BEGIN " << __func__ << endl);
        
        // Set ground coefficient
        ground_coeffs.resize(4);
        ground_coeffs[0] = 0.0362754;
        ground_coeffs[1] = 0.992727;
        ground_coeffs[2] = 0.114788;
        ground_coeffs[3] = -1.36448;



        pcl::fromROSMsg(input, *cloud);
        // Ground plane estimation:
        // Perform people detection on the new cloud:
        std::vector<pcl::people::PersonCluster<PointT> > clusters;   // vector containing persons clusters
        people_detector.setInputCloud(cloud);
        people_detector.setGround(ground_coeffs);                    // set floor coefficients
        people_detector.compute(clusters);                           // perform people detection
        ground_coeffs = people_detector.getGround();                 // get updated floor coefficients

        // unsigned int k = 0;
        // Eigen::Vector3f c;
        // for (std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it)
        // {
        //     // if (it->getPersonConfidence() > min_confidence)             // draw only people with confidence above a threshold
        //     // {
        //         k++;
        //     // }
        // }

        // viewer.removeAllPointClouds();
        // viewer.removeAllShapes();
        // pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
        // viewer.addPointCloud<PointT> (cloud, rgb, "input_cloud");

        unsigned int k = 0;
        
        visualization_msgs::MarkerArray markerArray;

        for(std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it){
            // if(it->getPersonConfidence() > min_confidence) {           
            // draw only people with confidence above a threshold
            // draw theoretical person bounding box in the PCL viewer:
            // it->drawTBoundingBox(viewer, k);

            visualization_msgs::Marker marker = gen_marker_from_cluster(*it,cloud);
            // Set marker header
            marker.header.frame_id = "/camera_rgb_optical_frame";
            marker.header.stamp = ros::Time::now();
            marker.ns = "my_points";
            marker.action = visualization_msgs::Marker::ADD;
            marker.pose.orientation.w = 1.0;
            marker.id = k;
            marker.type = visualization_msgs::Marker::POINTS;
            marker.lifetime = ros::Duration(1);
            // POINTS markers use x and y scale for width/height respectively
            marker.scale.x = 0.1;
            marker.scale.y = 0.1;

            // Marker color
            marker.color.r = (float) k/(float) clusters.size();
            marker.color.b = 1.0f;
            marker.color.a = 1.0f;

            markerArray.markers.push_back(marker);
            // Push marker into marker array

            // Make text header
            visualization_msgs::Marker marker_text;
            // Set marker header
            marker_text.header.frame_id = "/camera_rgb_optical_frame";
            marker_text.header.stamp = ros::Time::now();
            marker_text.ns = "text";
            marker_text.action = visualization_msgs::Marker::ADD;
            // marker_text.pose.orientation.w = 1.0;
            marker_text.id = k;
            marker_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            marker_text.lifetime = ros::Duration(1);
            // POINTS markers use x and y scale for width/height respectively
            marker_text.scale.z = 0.2;
            std::stringstream ss;
            ss << "k = " << k << " Confidence:" << it->getPersonConfidence() << std::endl;
            marker_text.text = ss.str();

            Eigen::Vector3f topPoint = it->getTop();
            marker_text.pose.position.x = topPoint[0];
            marker_text.pose.position.y = topPoint[1];
            marker_text.pose.position.z = topPoint[2];

            marker_text.color.r = 1.0f;
            marker_text.color.b = 1.0f;
            marker_text.color.g = 1.0f;
            marker_text.color.a = 1.0f;

            markerArray.markers.push_back(marker_text);

            ROS_INFO_STREAM("k = " << k << " Confidence = " << it->getPersonConfidence());

            k++;
            // }
        }
        ROS_INFO_STREAM(k << " people found" << endl);
        // viewer.spinOnce();

        // Publish markerArray
        pub_marker.publish(markerArray);

        // ROS_INFO_STREAM("END " << __func__ << endl);
    }

    visualization_msgs::Marker gen_marker_from_cluster(pcl::people::PersonCluster<PointT> pplcluster, const pcl::PointCloud<PointT>::Ptr oriCloud){
        // Return a marker for visualization given a person cluster
        visualization_msgs::Marker marker;
        Eigen::Vector3f topPoint = pplcluster.getTop();
        Eigen::Vector3f botPoint = pplcluster.getBottom();
        Eigen::Vector3f centerPoint = pplcluster.getCenter();

        geometry_msgs::Point p_top,p_bot,p_cen;
        p_top.x = topPoint[0];
        p_top.y = topPoint[1];
        p_top.z = topPoint[2];

        p_bot.x = botPoint[0];
        p_bot.y = botPoint[1];
        p_bot.z = botPoint[2];

        p_cen.x = centerPoint[0];
        p_cen.y = centerPoint[1];
        p_cen.z = centerPoint[2];

        marker.points.push_back(p_top);
        marker.points.push_back(p_bot);
        marker.points.push_back(p_cen);



        // for (int i = 0; i < pplPointCloud.points.size(); i++){
        //     geometry_msgs::Point p;
        //     p.x = pplPointCloud.points[i].x;
        //     p.y = pplPointCloud.points[i].y;
        //     p.y = pplPointCloud.points[i].z;
        //     // p.x = topPoint[0];
        //     // p.y = topPoint[1];
        //     // p.z = topPoint[2];
        //     marker.points.push_back(p);
        // }



        // pcl::PointIndices pointIndices = pplcluster.getIndices();
        // for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++){
        //     // if(!pcl::isFinite((float) oriCloud->points[*pit].x) || !pcl::isFinite((float) oriCloud->points[*pit].x) || !pcl::isFinite((float) oriCloud->points[*pit].x)){
        //     if (!pcl::isFinite(oriCloud->points[*pit])){
        //         // If the point for the cloud is not valid, don't push it into the marker points.
        //         continue; 
        //     }
        //     geometry_msgs::Point p;
        //     p.x = (float) oriCloud->points[*pit].x;
        //     p.y = (float) oriCloud->points[*pit].y;
        //     p.z = (float) oriCloud->points[*pit].z;
        //     marker.points.push_back(p);
        // }


        return marker;
    }


};//class PeopleTracker

int main (int argc, char** argv)
{
    // if(pcl::console::find_switch (argc, argv, "--help") || pcl::console::find_switch (argc, argv, "-h"))
    //     return print_help();

    string svm_filename;
    float min_confidence = -20;
    float min_height = 1.0;
    float max_height = 3.0;
    float voxel_size = 0.06;
 
    // Read if some parameters are passed from command line:
    // pcl::console::parse_argument (argc, argv, "--svm", svm_filename);
    // pcl::console::parse_argument (argc, argv, "--conf", min_confidence);
    // pcl::console::parse_argument (argc, argv, "--min_h", min_height);
    // pcl::console::parse_argument (argc, argv, "--max_h", max_height);


    ros::init (argc, argv, "people_tracker");
    PeopleTracker ptracker = PeopleTracker(argc, argv, svm_filename, min_confidence, min_height, max_height, voxel_size);

    ros::spin();

    return 0;
} 