#include <ros/ros.h>

// For ROS to interact with PCL
#include <utilpcl.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/common.h>


class GroundRemove{
public:
  ros::NodeHandle nh;
  ros::Publisher pub;
  ros::Subscriber sub;
  tf::TransformListener tfListener;
  pcl::ModelCoefficients::Ptr ground_plan_coefficient;

  GroundRemove(int argc, char** argv)
  {
    // Setup node handler
    nh = ros::NodeHandle("~");
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
    sub = nh.subscribe("input", 5, &GroundRemove::class_callback, this);

    // Set default parameters
    // if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world"); }
    // if (!ros::param::has("~z_remove_ratio")) { ros::param::set("~z_remove_ratio",0.1); }

    if (!ros::param::has("~max_iteration")) { ros::param::set("~max_iteration",500); };
    if (!ros::param::has("~distance_threshold")) { ros::param::set("~distance_threshold",0.1); };

  }

  void class_callback(const sensor_msgs::PointCloud2& input)
  {
    // Convert input to pcl::PointCloud format
    // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    // pcl::fromROSMsg (input, *cloud);

    // Check parameter
    // std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);
    // double z_remove_ratio; ros::param::getCached("~z_remove_ratio",z_remove_ratio);

    // Transform into goal frame
    // sensor_msgs::PointCloud2 cloud_tfed;

    // tfListener.waitForTransform(goal_frame, input.header.frame_id, ros::Time(0), ros::Duration(0.01));
    // pcl_ros::transformPointCloud(goal_frame,input,cloud_tfed,tfListener);

    // Convert to pcl::PointCloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (input, *cloud);
    // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_down = utilpcl::downsample(cloud, 0.4);
    
    int max_iteration; ros::param::getCached("~max_iteration",max_iteration);
    double distance_threshold; ros::param::getCached("~distance_threshold",distance_threshold);
    
    // Extract the ground plane coefficient  from the downsampled cloud
    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr ground_indices = utilpcl::get_planar_indicies_ransac(cloud, max_iteration, distance_threshold, coefficient);
    // ROS_INFO_STREAM("coefficient->values.size() = " << coefficient->values.size());

    if (coefficient->values.size() > 0){ //This means that the coefficient extraction is successful 
      // Use the groud plane coefficient to remove the ground in the original cloud
      this->ground_plan_coefficient = coefficient;
      // pcl::PointIndices::Ptr ground_indices = utilpcl::get_plane_inlier_indicies(cloud, coefficient, 0.1);
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_top = utilpcl::extract_pointcloud_by_indices(cloud, ground_indices, true);
      
      // // Filter based on height (Keep top 90% of points)
      // pcl::PointXYZ minPoint;
      // pcl::PointXYZ maxPoint;
      // pcl::getMinMax3D(*cloud,minPoint,maxPoint);
      // double z_min = minPoint.z;
      // double z_max = maxPoint.z;
      // double z_cut = (z_max-z_min)*z_remove_ratio + z_min;
      // pcl::PointIndices::Ptr passIndices = utilpcl::get_passthrough_indicies(cloud,"z",z_cut,z_max);
      // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_top = utilpcl::extract_pointcloud_by_indices(cloud,passIndices);

      // ROS_INFO_STREAM("Removed " << cloud->size() - cloud_top->size() << " points.");
      // ROS_INFO_STREAM("z_min = " << z_min << " z_cut = " << z_cut << " z_max = " << z_max);

      // Convert and publish the filtered point cloud
      sensor_msgs::PointCloud2 cloud_pub;
      pcl::toROSMsg(*cloud_top, cloud_pub);
      // Put the processed cloud under the same frame as the tfed cloud
      cloud_pub.header.frame_id = input.header.frame_id; 
      cloud_pub.header.stamp = input.header.stamp;
      pub.publish(cloud_pub);
    }


  }
};

int main (int argc, char**argv)
{
  ros::init (argc, argv, "ground_remove");
  GroundRemove groundRemove(argc, argv);
  ros::spin();
  return 0;
}