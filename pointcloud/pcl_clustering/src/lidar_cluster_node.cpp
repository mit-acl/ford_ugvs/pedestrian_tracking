#include <ros/ros.h>

#include <stdlib.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/common/common.h>

#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>

#include <utilpcl.h>

typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, sensor_msgs::LaserScan> MySyncPolicy;
class SyncTest{
public:

  struct rgb{
    double r;
    double g;
    double b;
  };

  ros::NodeHandle nh;
  message_filters::Subscriber<sensor_msgs::LaserScan> image1_sub;
  message_filters::Subscriber<sensor_msgs::LaserScan> image2_sub;
  message_filters::Synchronizer<MySyncPolicy>* sync;

  ros::Publisher pub_cloud;
  ros::Publisher pub_marker;

  tf::TransformListener* tfListener;
  laser_geometry::LaserProjection projector;

  std::vector<rgb> colormap;

  SyncTest(int argc, char** argv)
  {
    nh = ros::NodeHandle("~");
    image1_sub.subscribe(nh,"input_1",2);
    image2_sub.subscribe(nh,"input_2",2);
    sync = new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(10), image1_sub, image2_sub);
    sync->registerCallback(boost::bind(&SyncTest::callback,this, _1, _2));

    tfListener = new tf::TransformListener(ros::Duration(60));

    // Advertise Publishment
    pub_cloud = nh.advertise<sensor_msgs::PointCloud2> ("pointcloud",5);
    pub_marker = nh.advertise<visualization_msgs::MarkerArray> ("marker_array",10);

    // Set default parameters
    if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world");}

    // Cluster Parameters
    if (!ros::param::has("~cluster/dist_tolerance")) { ros::param::set("~cluster/dist_tolerance",0.15);}
    if (!ros::param::has("~cluster/min_size")) { ros::param::set("~cluster/min_size",5);}
    if (!ros::param::has("~cluster/max_size")) { ros::param::set("~cluster/max_size",10000);}
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr laserScanToPointCloud(const sensor_msgs::LaserScan::ConstPtr& laserScan, std::string goal_frame)
  {
    // Convert LaserScan msg to pointcloud. Transform into the goal_frame specified
    sensor_msgs::PointCloud2::Ptr lidar_msg(new sensor_msgs::PointCloud2);

    tfListener->waitForTransform(goal_frame, laserScan->header.frame_id, ros::Time(0), ros::Duration(20.0));
    projector.projectLaser(*laserScan, *lidar_msg);

    sensor_msgs::PointCloud2::Ptr lidar_msg_tf(new sensor_msgs::PointCloud2);
    pcl_ros::transformPointCloud(goal_frame,*lidar_msg,*lidar_msg_tf,*tfListener);

    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_pointCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (*lidar_msg_tf,*lidar_pointCloud);

    return lidar_pointCloud;
  }

  void callback(const sensor_msgs::LaserScan::ConstPtr& input_top, const sensor_msgs::LaserScan::ConstPtr& input_bot)
  {
    
    // ROS_INFO_STREAM("Class Sync Called!!!");
    std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_top = laserScanToPointCloud(input_top,goal_frame);
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_bot = laserScanToPointCloud(input_bot,goal_frame);

    // ROS_INFO_STREAM("lidar_top.size()" << lidar_top->size());
    // ROS_INFO_STREAM("lidar_bot.size()" << lidar_bot->size());

    // Combine them into the same pointcloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr lidar(new pcl::PointCloud<pcl::PointXYZ>);
    *lidar = *lidar_top + *lidar_bot;
    // ROS_INFO_STREAM("lidar.size()" << lidar->size());

    // Project them on the floor
    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients);
    coefficient->header = lidar_top->header;
    coefficient->values.push_back(-0.00627247);
    coefficient->values.push_back(-0.00825334);
    coefficient->values.push_back(0.999946);
    coefficient->values.push_back(-0.0295839); // coefficient->values.push_back(-1.0);
    lidar = utilpcl::project_pointcloud(lidar, coefficient);

    // Clustering
    double cluster_dist_tolerance; ros::param::getCached("~cluster/dist_tolerance",cluster_dist_tolerance);
    int min_cluster_size; ros::param::getCached("~cluster/min_size",min_cluster_size);
    int max_cluster_size; ros::param::getCached("~cluster/max_size",max_cluster_size);
    std::vector<pcl::PointIndices> clusterVec = utilpcl::get_euclidean_cluster_indices(lidar, cluster_dist_tolerance, min_cluster_size, max_cluster_size);

    // Generate markerArray for visualization
    visualization_msgs::MarkerArray markerArray;
    int j = 0;

    for (std::vector<pcl::PointIndices>::const_iterator indices = clusterVec.begin(); indices != clusterVec.end(); ++indices)
    {
      // Generate the markers for each cluster
      std::vector<visualization_msgs::Marker> markers = gen_markers_from_cluster(lidar, *indices, j, goal_frame);
      //Append the markers to the marker array
      markerArray.markers.insert(markerArray.markers.end(), markers.begin(), markers.end()); 
      j++;
    }

    pub_marker.publish(markerArray);

    sensor_msgs::PointCloud2::Ptr cloud_pub(new sensor_msgs::PointCloud2);
    pcl::toROSMsg(*lidar, *cloud_pub);
    cloud_pub->header.frame_id = goal_frame; 
    cloud_pub->header.stamp = ros::Time::now();
    pub_cloud.publish(cloud_pub);
  }


  rgb get_color_by_id(int id)
  {
    // Return color by id. If no color is defined for id. Generate a random color

    // Fill colormap if empty
    if ( colormap.size() < (id + 1) ){
      rgb randomColor = { (float)(rand()%10)/10.0, (float)(rand()%10)/10.0, (float)(rand()%10)/10.0 };
      colormap.push_back(randomColor);
    }
    return colormap[id];
  }

  std::vector<visualization_msgs::Marker> gen_markers_from_cluster(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices, int id, std::string goal_frame)
  {
    std::vector<visualization_msgs::Marker> markerVector;
    
    // The points
    visualization_msgs::Marker marker_points = gen_marker_points_from_indices(oriCloud, pointIndices);
    marker_points.header.frame_id = goal_frame;
    marker_points.header.stamp = ros::Time::now();
    marker_points.ns = ros::this_node::getName();
    marker_points.action = visualization_msgs::Marker::ADD;
    marker_points.id = id;
    marker_points.lifetime = ros::Duration(0.5);
    marker_points.pose.orientation.w = 1.0;
    marker_points.type = visualization_msgs::Marker::POINTS;
    marker_points.scale.x = 0.05;
    marker_points.scale.y = 0.05;
    // Marker color
    
    rgb randomColor = get_color_by_id(id);
    marker_points.color.r = randomColor.r;
    marker_points.color.g = randomColor.g;
    marker_points.color.b = randomColor.b;
    marker_points.color.a = 1.0f;
    markerVector.push_back(marker_points);

    // The text
    visualization_msgs::Marker marker_text;
    marker_text.header.frame_id = goal_frame;
    marker_text.header.stamp = ros::Time::now();
    marker_text.ns = "text";
    marker_text.action = visualization_msgs::Marker::ADD;
    marker_text.id = id;
    marker_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    marker_text.lifetime = ros::Duration(0.5);
    marker_text.scale.z = 0.2; //Size of text
    std::stringstream ss;
    ss << "id: " << id << std::endl;
    marker_text.text = ss.str();
    pcl::PointXYZ meanPoint = get_mean_point_from_indices(oriCloud, pointIndices);
    marker_text.pose.position.x = meanPoint.x;
    marker_text.pose.position.y = meanPoint.y;
    marker_text.pose.position.z = meanPoint.z;
    marker_text.color.r = 1.0f;
    marker_text.color.b = 1.0f;
    marker_text.color.g = 1.0f;
    marker_text.color.a = 1.0f;
    markerVector.push_back(marker_text);

    return markerVector;
  }


  visualization_msgs::Marker gen_marker_points_from_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices)
  {
    // Return visualization maker from point cloud given indices
    visualization_msgs::Marker marker;
    for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++)
    {
      geometry_msgs::Point p;
      p.x = (float) oriCloud->points[*pit].x;
      p.y = (float) oriCloud->points[*pit].y;
      p.z = (float) oriCloud->points[*pit].z;
      marker.points.push_back(p);
    }
    return marker;
  }

  pcl::PointXYZ get_mean_point_from_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices)
  {
    // Return visualization maker from point cloud given indices
    pcl::PointXYZ meanPoint;
    double xsum = 0; double ysum = 0; double zsum = 0;
    for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++)
    {
      xsum += (float) oriCloud->points[*pit].x;
      ysum += (float) oriCloud->points[*pit].y;
      zsum += (float) oriCloud->points[*pit].z;
    }
    // pcl::PointXYZ meanPoint(xsum/(float) pointIndices.indices.size(),ysum/(float) pointIndices.indices.size(), zsum/(float) pointIndices.indices.size());
    meanPoint.x = xsum/(float) pointIndices.indices.size();
    meanPoint.y = ysum/(float) pointIndices.indices.size();
    meanPoint.z = zsum/(float) pointIndices.indices.size();
    return meanPoint;
  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "vision_node");
  SyncTest syncTest(argc,argv);

  ros::spin();
  return 0;
}