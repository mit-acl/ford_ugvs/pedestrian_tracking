#include <ros/ros.h>

// #include <stdlib.h>

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl_ros/transforms.h>

#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>

// #include <utilpcl.hpp>

class LidarToCloud{
public:

  ros::NodeHandle nh;
  ros::Subscriber sub;
  ros::Publisher pub_cloud;
  
  // tf::TransformListener* tfListener;
  laser_geometry::LaserProjection projector;
  // std::string goal_frame;

  LidarToCloud(int argc, char** argv)
  {
    nh = ros::NodeHandle("~");
    pub_cloud = nh.advertise<sensor_msgs::PointCloud2> ("output",1);
    sub = nh.subscribe("input", 1, &LidarToCloud::callback, this);
  }
  void callback(const sensor_msgs::LaserScan::ConstPtr& laserScan)
  {
    // Convert LaserScan msg to pointcloud. Transform into the goal_frame specified
    sensor_msgs::PointCloud2::Ptr lidar_cloud(new sensor_msgs::PointCloud2);
    // Convert to a pointcloud msgs
    projector.projectLaser(*laserScan, *lidar_cloud);
    // Transform the cloud to goal_frame
    pub_cloud.publish(lidar_cloud);
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "lidar_to_cloud");
  LidarToCloud lidarObj(argc,argv);
  ros::spin();
  return 0;
}