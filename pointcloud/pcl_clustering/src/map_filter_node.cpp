#include <ros/ros.h>
#include <vector>

/* PCL */
#include <utilpcl.hpp>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>

/* Msgs */
#include <sensor_msgs/PointCloud2.h>
#include "map_server/image_loader.h"
#include "nav_msgs/MapMetaData.h"

#define MAP_INDEX(size_x, i, j) ((i) + (j) * size_x)

class MapFilter{
public:

  ros::NodeHandle nh_;

  ros::Publisher pub_cloud_;
  ros::Publisher pub_map_;
  
  ros::Subscriber sub_cloud_;
  ros::Subscriber sub_map_;
  
  tf::TransformListener tf_listener_;
  
  pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>* octree_;

  pcl::PointCloud<pcl::PointXYZ> map_cloud_;

  bool has_key_cloud;

  /*Parameters*/
  std::string map_frame_;
  bool noise_remove_flag_;
  bool diff_filter_flag_;
  double octree_res_;
  double noise_remove_radius_;
  int noise_remove_min_neighbor_;

  MapFilter(int argc, char** argv)
  {
    // Setup node handler
    nh_ = ros::NodeHandle("~");
    
    setDefaultParam();
    getParam();

    octree_ = new pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>(octree_res_);
    has_key_cloud = false;

    sub_cloud_ = nh_.subscribe("input", 2, &MapFilter::cbCloud, this);
    sub_map_ = nh_.subscribe("map", 1, &MapFilter::cbMap, this);
    pub_cloud_ = nh_.advertise<sensor_msgs::PointCloud2> ("output",1);
    pub_map_ = nh_.advertise<sensor_msgs::PointCloud2> ("map_cloud",1,true); //latching
  }

  void setDefaultParam()
  {
    if (!ros::param::has("~map_frame")) { ros::param::set("~map_frame","map");}
    if (!ros::param::has("~diff_filter/flag")) { ros::param::set("~diff_filter/flag",true);}
    if (!ros::param::has("~diff_filter/octree_res")) { ros::param::set("~diff_filter/octree_res",0.5);}
    if (!ros::param::has("~noise_remove/flag")) { ros::param::set("~noise_remove/flag",true);}
    if (!ros::param::has("~noise_remove/radius")) { ros::param::set("~noise_remove/radius",0.5);}
    if (!ros::param::has("~noise_remove/min_neighbor")) { ros::param::set("~noise_remove/min_neighbor",3);}
  }

  void getParam()
  {
    ros::param::getCached("~map_frame",map_frame_);
    ros::param::getCached("~diff_filter/flag",diff_filter_flag_);
    ros::param::getCached("~diff_filter/octree_res",octree_res_);
    ros::param::getCached("~noise_remove/flag",noise_remove_flag_);
    ros::param::getCached("~noise_remove/radius",noise_remove_radius_);
    ros::param::getCached("~noise_remove/min_neighbor",noise_remove_min_neighbor_);
  }

  void cbMap(const nav_msgs::OccupancyGridConstPtr& msg)
  {
    if(has_key_cloud) {
      ROS_INFO_STREAM("[MapFilter] Map received, but key cloud is already set");
      return;
    }

    ROS_INFO_STREAM("[MapFilter] Reading the map");
    const nav_msgs::OccupancyGrid& map_msg = *msg;
    //Add points from OccupancyGrid to the map pointcloud
    for(int i = 0; i < map_msg.info.width; i++)
      for(int j = 0; j < map_msg.info.height; j++)
        if(map_msg.data[MAP_INDEX(map_msg.info.width,i,j)] == 100) //Why 100?
          map_cloud_.push_back(pcl::PointXYZ(i*map_msg.info.resolution+map_msg.info.origin.position.x, j*map_msg.info.resolution+map_msg.info.origin.position.y, 0.0));

    // Convert pointcloud to Ptr and load it
    loadKeyCloud();
    has_key_cloud = true;

    // Publish the map cloud
    sensor_msgs::PointCloud2 map_cloud_pub;
    pcl::toROSMsg(map_cloud_, map_cloud_pub);    
    map_cloud_pub.header.frame_id = map_frame_;
    pub_map_.publish(map_cloud_pub);
  }

  void loadKeyCloud(){
    // Load the key cloud into the octree
    octree_->deleteCurrentBuffer();
    octree_->switchBuffers();
    octree_->setInputCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr(&map_cloud_));
    octree_->addPointsFromInputCloud();
    // octree->switchBuffers();
    ROS_INFO_STREAM("[MapFilter] Key cloud loaded.");
  }

  void getDiffToKey(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
  {
    if (not has_key_cloud){
      ROS_INFO_STREAM("[MapFilter] No key cloud yet.");
      return;
    }
    // Use extract indices to extract the different pointcloud
    pcl::PointIndices::Ptr diffIndices (new pcl::PointIndices ());
    for (int i = 0; i < cloud->points.size();i++){
      if(not octree_->isVoxelOccupiedAtPoint(cloud->points[i])){
        diffIndices->indices.push_back(i);
      }
    }
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud (cloud);
    extract.setIndices (diffIndices);
    extract.setNegative (false);
    extract.filter (*cloud);
  }

  void cbCloud(const sensor_msgs::PointCloud2& input)
  {
    //Transform pointcloud msg to map frame
    sensor_msgs::PointCloud2 input_transformed;
    if (!tf_listener_.waitForTransform(map_frame_, input.header.frame_id, input.header.stamp, ros::Duration(1))){
      return;
    }
    pcl_ros::transformPointCloud(map_frame_,input,input_transformed, tf_listener_); 

    /* Convert the transformed cloud to pcl::PointCloud format */
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::fromROSMsg (input_transformed, *cloud_in);

    /* Remove NaN */
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
    std::vector<int> temp_ind;
    pcl::removeNaNFromPointCloud(*cloud_in,*cloud,temp_ind);

    /* Move cloud to the ground */
    for(pcl::PointCloud<pcl::PointXYZ>::iterator it = cloud->begin(); it != cloud->end(); ++it){
      it->z = 0.0;
    }

    /* Apply the map filter */
    if (diff_filter_flag_){
      getDiffToKey(cloud);
    }

    /* Make sure remaining cloud isn't empty */
    if(cloud->size()==0){
      ROS_INFO_STREAM("[MapFilter] Empty map cloud -- returning w/o publishing.");
      return;
    }

    /* Noise removal */
    if (noise_remove_flag_){
      cloud = utilpcl::remove_radius_outlier(cloud, noise_remove_radius_, noise_remove_min_neighbor_);
      if(cloud->size()==0){
        ROS_INFO_STREAM("[MapFilter] Empty cloud after noise removal.");
        return;
      }
    }

    // ===Convert to ROSMsg and publish===
    sensor_msgs::PointCloud2 cloud_pub;
    pcl::toROSMsg(*cloud, cloud_pub);
    pub_cloud_.publish(cloud_pub);
  }
};


int main (int argc, char**argv)
{
  ros::init (argc, argv, "map_filter");  
  MapFilter cloudFliterObj(argc, argv);
  ros::spin();
  return 0;
}
