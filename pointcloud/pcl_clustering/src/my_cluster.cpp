#include <ros/ros.h>
#include <vector>
#include <stdlib.h> 
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
// rviz includes
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>


ros::Publisher pub;
ros::Publisher pub_marker;


pcl::PointCloud<pcl::PointXYZ>::Ptr downsample(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud){
	// Read parameters
	double leafSize;
	// ros::NodeHandle nh;
	ros::param::getCached("~downsample/leafsize",leafSize);
	// nh.getParamCached("downsample/leafsize",leafSize);

	// Downsample the cloud using VoxelGrid
	pcl::VoxelGrid<pcl::PointXYZ> vg;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	vg.setInputCloud (oriCloud->makeShared ());
	vg.setLeafSize (leafSize,leafSize,leafSize);
	vg.filter (*cloud_filtered);
	return cloud_filtered;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr planer_seg(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud){
	// Filter out the part of the cloud that can be planer segmated and return the leftovers.
	

	// Get parameters
	int max_iteration; ros::param::getCached("~planar_seg/max_iteration",max_iteration);
	double distance_threshold; ros::param::getCached("~planar_seg/distance_threshold",distance_threshold);
	double keep_point_ratio; ros::param::getCached("~planar_seg/keep_point_ratio",keep_point_ratio);

	// Create the segmentation object for the planar model and set all the parameters
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_for_seg = oriCloud->makeShared(); //Make a copy of the input cloud
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZ> ());

	// Sepcify segmentation parameters
	seg.setOptimizeCoefficients (true);
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (max_iteration);
	seg.setDistanceThreshold (distance_threshold);

	int i=0, nr_points = (int) cloud_for_seg->points.size ();
	while (cloud_for_seg->points.size () > keep_point_ratio * nr_points){
		// Segment the largest planar component from the remaining cloud
		seg.setInputCloud (cloud_for_seg);
		seg.segment (*inliers, *coefficients);
		if (inliers->indices.size () == 0)
		{
		  std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
		  break;
		}

		// Extract the planar inliers from the input cloud
		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud (cloud_for_seg);
		extract.setIndices (inliers);
		extract.setNegative (false);

		// Get the points associated with the planar surface
		extract.filter (*cloud_plane);
		// std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

		// Remove the planar inliers, extract the rest
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>); //This line was missing in the tutorial
		extract.setNegative (true); //cloud_f contains all the points in the cloud_for_seg that is not an inlier
		extract.filter (*cloud_f);
		*cloud_for_seg = *cloud_f;
	}
	return cloud_for_seg;
}

std::vector<pcl::PointIndices> get_euclidean_cluster_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud){
	// Return the indices of the Euclidean Clusters

	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (oriCloud);

	std::vector<pcl::PointIndices> cluster_indices;

	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

	double cluster_tolerance; ros::param::getCached("~euclidean_cluster/cluster_tolerance", cluster_tolerance);

	ec.setClusterTolerance (cluster_tolerance); // in meter
	ec.setMinClusterSize (100);
	ec.setMaxClusterSize (25000);
	ec.setSearchMethod (tree);
	ec.setInputCloud (oriCloud);

	ec.extract (cluster_indices);

	return cluster_indices;	
}

visualization_msgs::Marker gen_raw_marker_from_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices){
	// Return visualization maker from point cloud given indices
	visualization_msgs::Marker marker;
	for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++){
		geometry_msgs::Point p;
		p.x = (float) oriCloud->points[*pit].x;
		p.y = (float) oriCloud->points[*pit].y;
		p.z = (float) oriCloud->points[*pit].z;
    	marker.points.push_back(p);
	}
	return marker;
}

visualization_msgs::MarkerArray gen_marker_array(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const std::vector<pcl::PointIndices> indicesVec){
	visualization_msgs::MarkerArray markerArray;
	int j = 0;
	for (std::vector<pcl::PointIndices>::const_iterator indices = indicesVec.begin(); indices != indicesVec.end(); ++indices){
		// Generate marker
		visualization_msgs::Marker marker = gen_raw_marker_from_indices(oriCloud, *indices);
	    
	    // Fill in marker data
	    marker.header.frame_id = "/velodyne";
	    marker.header.stamp = ros::Time::now();
	    marker.ns = "my_points";
	    marker.action = visualization_msgs::Marker::ADD;
	    marker.pose.orientation.w = 1.0;
	    marker.id = j;
	    marker.type = visualization_msgs::Marker::POINTS;

	    // POINTS markers use x and y scale for width/height respectively
	    marker.scale.x = 0.1;
	    marker.scale.y = 0.1;

	    // Marker color
	    marker.color.r = (float) j/(float) indicesVec.size();
	    marker.color.b = 1.0f;

	    marker.color.a = 1.0f;

	    markerArray.markers.push_back(marker);
	    j++;
	}
	return markerArray;
}

void my_cluster_callback (const sensor_msgs::PointCloud2ConstPtr& input){
	// Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud<pcl::PointXYZ>
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromROSMsg (*input, *cloud);


	std::cout << "Original cloud has: " << cloud->points.size ()  << " data points." << std::endl; //*

	// Down sample the input cloud
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_down = downsample(cloud);
	std::cout << "PointCloud after filtering has: " << cloud_down->points.size ()  << " data points." << std::endl; //*


	// Use iterative planer segmentation to remove backgroud such as floor and walls
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_seged = planer_seg(cloud_down);
	std::cout << "PointCloud after segmentation has: " << cloud_seged->points.size ()  << " data points." << std::endl; //*

	// Extract clusters
	std::vector<pcl::PointIndices> indices_euc_clusters = get_euclidean_cluster_indices(cloud_seged);
	std::cout << "Number of clusters: " <<  indices_euc_clusters.size() << std::endl; //*

	// Generate Markers for visualization
	visualization_msgs::MarkerArray markerArray = gen_marker_array(cloud_seged,indices_euc_clusters);
	// std::cout << "Size of markerArray:" << markerArray.markers.size() << std::endl;
	pub_marker.publish(markerArray);

	// Convert the resulting pointcloud back to sensor_msgs and publish
    sensor_msgs::PointCloud2 cloud_pub;
	pcl::toROSMsg(*cloud_seged, cloud_pub);
	// pcl::toROSMsg(*cloud_down, cloud_pub);
	cloud_pub.header.frame_id = input->header.frame_id; // Put the processed cloud under the same frame as the input
	pub.publish(cloud_pub);
}


int main (int argc, char** argv){
	// Initialize ROS
	ros::init (argc, argv, "my_cluster");
	ros::NodeHandle nh("~");

	// Set default parameters
	ros::param::set("~downsample/leafsize",0.1);
	ros::param::set("~planar_seg/max_iteration",500);
	ros::param::set("~planar_seg/distance_threshold",0.1);
	ros::param::set("~planar_seg/keep_point_ratio",0.9);
	ros::param::set("~euclidean_cluster/cluster_tolerance",0.4);

	// nh.setParam("downsample/leafsize",0.05);
	// nh.setParam("planar_seg/max_iteration",50);
	// nh.setParam("planar_seg/distance_threshold",0.05);
	// nh.setParam("planar_seg/keep_point_ratio",0.3);
	// nh.setParam("euclidean_cluster/cluster_tolerance",0.2);

	// Create a ROS subscriber for the input point cloudoutpu
	ros::Subscriber sub = nh.subscribe("input", 10, my_cluster_callback);
	// Create a ROS publisher for the output model coefficients  
	pub_marker = nh.advertise<visualization_msgs::MarkerArray> ("output_markerarray",10);
	pub = nh.advertise<sensor_msgs::PointCloud2> ("output_pointcloud2",10);

	// Spin
	ros::spin ();
}
