#include <vector>
#include <stdlib.h> 

#include <ros/ros.h>
#include <std_srvs/Empty.h>

// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/octree/octree.h>

#include <utilpcl.h>


class OctBackGroundRemove{
public:
	ros::NodeHandle nh;
	ros::Publisher pub;
	ros::Subscriber sub;
	ros::ServiceServer srv;
	pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>* octree;
	bool has_key_cloud;
	pcl::PointCloud<pcl::PointXYZ>::Ptr key_cloud;

	OctBackGroundRemove(int argc, char** argv, double octree_res)
	{
		// Setup node handler
		nh = ros::NodeHandle("~");
		pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
		sub = nh.subscribe("input", 5, &OctBackGroundRemove::class_callback, this);
		srv = nh.advertiseService("reset_key_cloud", &OctBackGroundRemove::srv_reset_key_cloud ,this);
		// Initialize octree
		octree = new pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ>(octree_res);
		has_key_cloud = false;

    if (!ros::param::has("~/noise_remove/flag")) { ros::param::set("~/noise_remove/flag",true);}
    if (!ros::param::has("~/noise_remove/radius")) { ros::param::set("~/noise_remove/radius",0.1);}
    if (!ros::param::has("~/noise_remove/min_neighbor")) { ros::param::set("~/noise_remove/min_neighbor",5);}
	}

	bool srv_reset_key_cloud(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
	{
		ROS_INFO_STREAM("srv_reset_key_cloud called");
		has_key_cloud = false;

		return true;
	}

	void set_key_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
	{	
		key_cloud = cloud;
		has_key_cloud = true;
		ROS_INFO_STREAM("Key cloud set.");
	}

	void load_key_cloud(){
		// Load the key cloud into the octree
		octree->deleteCurrentBuffer();
		octree->switchBuffers();
		octree->setInputCloud(key_cloud);
		octree->addPointsFromInputCloud();
		octree->switchBuffers();
		ROS_INFO_STREAM("Key cloud loaded.");
	}

	int get_diff_to_key(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out)
	{
		// Return the number of points in cloud_in that are not in the key cloud.
		// Put them in cloud_out
		octree->setInputCloud (cloud_in);
		octree->addPointsFromInputCloud ();
		// Get vector of point indices from octree voxels which did not exist in previous buffer
		std::vector<int> newPointIdxVector;
		octree->getPointIndicesFromNewVoxels (newPointIdxVector);

		// Use extract indices to extract the differ pointcloud
		pcl::PointIndices::Ptr diffIndices (new pcl::PointIndices ());
		diffIndices->indices = newPointIdxVector;

		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud (cloud_in);
		extract.setIndices (diffIndices);
		extract.setNegative (false);
		extract.filter (*cloud_out);

		// Remove cloud_in from current buffer
		octree->deleteCurrentBuffer();

		return newPointIdxVector.size();
	}

	void class_callback(const sensor_msgs::PointCloud2& input)
	{
		// Convert input to pcl::PointCloud format
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::fromROSMsg (input, *cloud);

		if (!has_key_cloud){
			// No key cloud yet then set current input as key cloud
			set_key_cloud(cloud);
			// Load it to buffer
			load_key_cloud();
		}
		else{
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff(new pcl::PointCloud<pcl::PointXYZ>);
			int size_of_diff = get_diff_to_key(cloud,cloud_diff);
			// ROS_INFO_STREAM("Size of diff: " << size_of_diff);

			double change_ratio = (double)size_of_diff/(double) cloud->size();
			if (change_ratio < 0.99){
				// Acceptable change rate.

				// Noise remvoal 
		    bool noise_remove_flag; ros::param::getCached("~/noise_remove/flag",noise_remove_flag);
		    if (noise_remove_flag){
		      double noise_remove_radius; ros::param::getCached("~/noise_remove/radius",noise_remove_radius);
		      int noise_remove_min_neighbor; ros::param::getCached("~/noise_remove/min_neighbor",noise_remove_min_neighbor);
		      cloud_diff = utilpcl::remove_radius_outlier(cloud_diff, noise_remove_radius, noise_remove_min_neighbor);
		      // ROS_INFO_STREAM("Size after noise remove: " << cloud_diff->size());
		    }

		    // Convert and publish
				sensor_msgs::PointCloud2 cloud_pub;
				pcl::toROSMsg(*cloud_diff, cloud_pub);
				cloud_pub.header.frame_id = input.header.frame_id; // Put the processed cloud under the same frame as the input
				cloud_pub.header.stamp = input.header.stamp;
				pub.publish(cloud_pub);
			}
			else{
				// Re-load the key cloud when change rate is too high
				load_key_cloud();
			}
		}
	} 
};


int main (int argc, char**argv)
{
	ros::init (argc, argv, "oct_background_remove");
	OctBackGroundRemove octBackGroundRemove(argc, argv, 0.2);
	ros::spin();
	return 0;
}