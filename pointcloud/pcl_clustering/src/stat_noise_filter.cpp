#include <vector>
#include <stdlib.h> 

#include <ros/ros.h>

// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>


class StatNoiseFilter{
public:
  ros::NodeHandle nh;
  ros::Publisher pub;
  ros::Subscriber sub;
  ros::ServiceServer srv;
  
  StatNoiseFilter(int argc, char** argv)
  {
    // Setup node handler
    nh = ros::NodeHandle("~");
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
    sub = nh.subscribe("input", 5, &StatNoiseFilter::class_callback, this);

    // Read parameters (or set default)
    if (!ros::param::has("~mean_k")) { ros::param::set("~mean_k",50);}
    if (!ros::param::has("~stddev_mul_thresh")) { ros::param::set("~stddev_mul_thresh",1.0);}

    // srv = nh.advertiseService("reset_key_cloud", &StatNoiseFilter::srv_reset_key_cloud ,this);
    // Initialize octree

  }

  void class_callback(const sensor_msgs::PointCloud2& input)
  {
    // Convert input to pcl::PointCloud format
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (input, *cloud);

    // Create the filtering object
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setInputCloud (cloud);

    // Handle and set parameters 
    int mean_k; ros::param::getCached("~mean_k",mean_k);
    double stddev_mul_thresh; ros::param::getCached("~stddev_mul_thresh",stddev_mul_thresh);

    sor.setMeanK (mean_k);
    sor.setStddevMulThresh (stddev_mul_thresh);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    sor.filter (*cloud_filtered);

    // Convert and publish the filtered point cloud
    sensor_msgs::PointCloud2 cloud_pub;
    pcl::toROSMsg(*cloud_filtered, cloud_pub);
    // Put the processed cloud under the same frame as the input
    cloud_pub.header.frame_id = input.header.frame_id; 
    cloud_pub.header.stamp = input.header.stamp;

    pub.publish(cloud_pub);
  }
};


int main (int argc, char**argv)
{
  ros::init (argc, argv, "stat_noise_filter");
  StatNoiseFilter StatNoiseFilter(argc, argv);
  ros::spin();
  return 0;
}