#include <ros/ros.h>

// For ROS to interact with PCL
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>

class TfToFrame{
public:
  ros::NodeHandle nh;
  ros::Publisher pub;
  ros::Subscriber sub;
  tf::TransformListener tfListener;
  
  TfToFrame(int argc, char** argv)
  {
    // Setup node handler
    nh = ros::NodeHandle("~");
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output",10);
    sub = nh.subscribe("input", 5, &TfToFrame::class_callback, this);

    // Set default parameters
    if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world"); }
  }

  void class_callback(const sensor_msgs::PointCloud2& input)
  {
    // Convert input to pcl::PointCloud format
    // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    // pcl::fromROSMsg (input, *cloud);

    // Check parameter
    std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);

    // Get the transform
    sensor_msgs::PointCloud2 cloud_pub;
    pcl_ros::transformPointCloud(goal_frame,input,cloud_pub,tfListener);
    // tflistener.transformPointCloud(goal_frame,input,cloud_pub);
    // // Convert and publish the filtered point cloud
    // sensor_msgs::PointCloud2 cloud_pub;
    // // pcl::toROSMsg(*cloud_filtered, cloud_pub);
    // // Put the processed cloud under the same frame as the input
    // cloud_pub.header.frame_id = input.header.frame_id; 
    // cloud_pub.header.stamp = input.header.stamp;
    pub.publish(cloud_pub);
  }

};


int main (int argc, char**argv)
{
  ros::init (argc, argv, "tf_to_frame");
  TfToFrame tfToFrame(argc, argv);
  ros::spin();
  return 0;
}