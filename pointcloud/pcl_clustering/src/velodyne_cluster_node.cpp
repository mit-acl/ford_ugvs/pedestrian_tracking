#include <ros/ros.h>
#include <dynmeans/dynmeans.hpp>

// For ROS to interact with PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl/common/common.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
// For using utilpcl
#include <utilpcl.h>

#include <cmath>

class VelodyneClusterClass{
public:
  ros::NodeHandle nh;
  ros::Publisher pub_marker;
  ros::Publisher pub_cloud;

  ros::Subscriber sub;
  tf::TransformListener tfListener;

  pcl::ModelCoefficients::Ptr ground_coefficient;

  DynMeans<Eigen::Vector2f>* dynm;

  struct rgb{
    double r;
    double g;
    double b;
  };
  std::vector<rgb> colormap;
  
  VelodyneClusterClass(int argc, char** argv)
  {
    // Setup node handler
    nh = ros::NodeHandle("~");
    pub_marker = nh.advertise<visualization_msgs::MarkerArray> ("output/marker_array",10);
    pub_cloud = nh.advertise<sensor_msgs::PointCloud2> ("output/pointcloud",5);
    sub = nh.subscribe("input", 5, &VelodyneClusterClass::class_callback, this);

    // Read parameters (or set default)

    // Transform frame
    if (!ros::param::has("~goal_frame")) { ros::param::set("~goal_frame","world");}

    // Downsample
    if (!ros::param::has("~downsample/leaf_size")) { ros::param::set("~downsample/leaf_size",0.2);}

    // Ground Remove
    if (!ros::param::has("~ground_remove/max_iteration")) { ros::param::set("~ground_remove/max_iteration",500); };
    if (!ros::param::has("~ground_remove/distance_threshold")) { ros::param::set("~ground_remove/distance_threshold",0.1); };
    if (!ros::param::has("~ground_remove/use_fixed_ground")) { ros::param::set("~ground_remove/use_fixed_ground",true); };

    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients());
    ground_coefficient = coefficient;
    ground_coefficient->values.push_back(-0.00627247);
    ground_coefficient->values.push_back(-0.00825334);
    ground_coefficient->values.push_back(0.999946);
    ground_coefficient->values.push_back(-0.0295839);

    // Cluster
    if (!ros::param::has("~cluster/dist_tolerance")) { ros::param::set("~cluster/dist_tolerance",0.18);}
    if (!ros::param::has("~cluster/min_size")) { ros::param::set("~cluster/min_size",50);}
    if (!ros::param::has("~cluster/max_size")) { ros::param::set("~cluster/max_size",2000);}

    // Projection
    if (!ros::param::has("~project_to_ground")) { ros::param::set("~project_to_ground",true);}

    // srv = nh.advertiseService("reset_key_cloud", &VelodyneClusterClass::srv_reset_key_cloud ,this);

    // Initialize dynmeans obj
    double lambda = 25.0; //0.05
    double T_Q = 50.0;  //6.8
    double K_tau = 1.01; //1.01

    double Q = lambda/T_Q;
    double tau = (T_Q*(K_tau-1.0)+1.0)/(T_Q-1.0);
    dynm = new DynMeans<Eigen::Vector2f>(lambda, Q, tau);

  }

  std::vector<visualization_msgs::Marker> gen_markers_from_cluster(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices, int id, std::string goal_frame)
  {
    std::vector<visualization_msgs::Marker> markerVector;
    
    // The points
    visualization_msgs::Marker marker_points = gen_marker_points_from_indices(oriCloud, pointIndices);
    marker_points.header.frame_id = goal_frame;
    marker_points.header.stamp = ros::Time::now();
    marker_points.ns = ros::this_node::getName();
    marker_points.action = visualization_msgs::Marker::ADD;
    marker_points.id = id;
    marker_points.lifetime = ros::Duration(0.5);
    marker_points.pose.orientation.w = 1.0;
    marker_points.type = visualization_msgs::Marker::POINTS;
    marker_points.scale.x = 0.05;
    marker_points.scale.y = 0.05;
    // Marker color
    
    rgb randomColor = get_color_by_id(id);
    marker_points.color.r = randomColor.r;
    marker_points.color.g = randomColor.g;
    marker_points.color.b = randomColor.b;
    marker_points.color.a = 1.0f;
    markerVector.push_back(marker_points);

    // The text
    visualization_msgs::Marker marker_text;
    marker_text.header.frame_id = goal_frame;
    marker_text.header.stamp = ros::Time::now();
    marker_text.ns = "text";
    marker_text.action = visualization_msgs::Marker::ADD;
    marker_text.id = id;
    marker_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    marker_text.lifetime = ros::Duration(0.5);
    marker_text.scale.z = 0.2; //Size of text
    std::stringstream ss;
    ss << "id: " << id << std::endl;
    marker_text.text = ss.str();
    pcl::PointXYZ meanPoint = get_mean_point_from_indices(oriCloud, pointIndices);
    marker_text.pose.position.x = meanPoint.x;
    marker_text.pose.position.y = meanPoint.y;
    marker_text.pose.position.z = meanPoint.z;
    marker_text.color.r = 1.0f;
    marker_text.color.b = 1.0f;
    marker_text.color.g = 1.0f;
    marker_text.color.a = 1.0f;
    markerVector.push_back(marker_text);


    // The box

    // Get 3dMinMax
    Eigen::Vector4f minPoint;
    Eigen::Vector4f maxPoint;
    pcl::getMinMax3D(*oriCloud,pointIndices.indices,minPoint,maxPoint);
    double radius = sqrt(pow(maxPoint[0]-minPoint[0],2) + pow(maxPoint[1]-minPoint[1],2))/2;
    double height = maxPoint[2] - minPoint[2];

    visualization_msgs::Marker marker_box;
    marker_box.header.frame_id = goal_frame;
    marker_box.header.stamp = ros::Time::now();
    marker_box.ns = "CUBE";
    marker_box.action = visualization_msgs::Marker::ADD;
    marker_box.id = id;
    marker_box.type = visualization_msgs::Marker::CUBE;
    marker_box.lifetime = ros::Duration(0.5);
    
    marker_box.scale.x = maxPoint[0] - minPoint[0];
    marker_box.scale.y = maxPoint[1] - minPoint[1];
    marker_box.scale.z = maxPoint[2] - minPoint[2];

    marker_box.pose.position.x = (maxPoint[0] + minPoint[0])/2;
    marker_box.pose.position.y = (maxPoint[1] + minPoint[1])/2;
    marker_box.pose.position.z = (maxPoint[2] + minPoint[2])/2;

    marker_box.color.r = 1.0f;
    marker_box.color.b = 1.0f;
    marker_box.color.g = 1.0f;
    marker_box.color.a = 0.3f;
    markerVector.push_back(marker_box);

    return markerVector;
  }

  visualization_msgs::Marker gen_marker_points_from_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices)
  {
    // Return visualization maker from point cloud given indices
    visualization_msgs::Marker marker;
    for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++)
    {
      geometry_msgs::Point p;
      p.x = (float) oriCloud->points[*pit].x;
      p.y = (float) oriCloud->points[*pit].y;
      p.z = (float) oriCloud->points[*pit].z;
      marker.points.push_back(p);
    }
    return marker;
  }

  pcl::PointXYZ get_mean_point_from_indices(const pcl::PointCloud<pcl::PointXYZ>::Ptr oriCloud, const pcl::PointIndices pointIndices)
  {
    // Return visualization maker from point cloud given indices
    pcl::PointXYZ meanPoint;
    double xsum = 0; double ysum = 0; double zsum = 0;
    for (std::vector<int>::const_iterator pit = pointIndices.indices.begin(); pit != pointIndices.indices.end(); pit++)
    {
      xsum += (float) oriCloud->points[*pit].x;
      ysum += (float) oriCloud->points[*pit].y;
      zsum += (float) oriCloud->points[*pit].z;
    }
    // pcl::PointXYZ meanPoint(xsum/(float) pointIndices.indices.size(),ysum/(float) pointIndices.indices.size(), zsum/(float) pointIndices.indices.size());
    meanPoint.x = xsum/(float) pointIndices.indices.size();
    meanPoint.y = ysum/(float) pointIndices.indices.size();
    meanPoint.z = zsum/(float) pointIndices.indices.size();
    return meanPoint;
  }

  void class_callback(const sensor_msgs::PointCloud2& input)
  {
    // Transform into goal frame
    std::string goal_frame; ros::param::getCached("~goal_frame",goal_frame);
    sensor_msgs::PointCloud2 cloud_tf;
    tfListener.waitForTransform(goal_frame, input.header.frame_id, ros::Time(0), ros::Duration(1));
    pcl_ros::transformPointCloud(goal_frame,input,cloud_tf,tfListener);

    // Convert input to pcl::PointCloud format
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (cloud_tf, *cloud);

    // Downsample
    double leaf_size; ros::param::getCached("~downsample/leaf_size",leaf_size);
    cloud = utilpcl::downsample(cloud,leaf_size);

    // Remove ground
    bool use_fixed_ground; ros::param::getCached("~ground_remove/use_fixed_ground",use_fixed_ground);
    double distance_threshold; ros::param::getCached("~ground_remove/distance_threshold",distance_threshold);
    pcl::PointIndices::Ptr ground_indices;
    pcl::ModelCoefficients::Ptr coefficient(new pcl::ModelCoefficients());
    ground_coefficient->header = cloud->header;

    if (use_fixed_ground){
      // Use the preset ground coefficient 
      coefficient = ground_coefficient;
      ground_indices = utilpcl::get_plane_inlier_indicies(cloud,ground_coefficient,distance_threshold);
    } 
    else{
      // Use ransac to get the coefficient of the ground
      int max_iteration; ros::param::getCached("~ground_remove/max_iteration",max_iteration);
      ground_indices = utilpcl::get_planar_indicies_ransac(cloud, max_iteration, distance_threshold, coefficient);
      // ROS_INFO_STREAM("Ground Coeff values:" << coefficient->values[0] << " " << coefficient->values[1] << " " << coefficient->values[2] << " "<< coefficient->values[3] << " " );
      // ROS_INFO_STREAM("Ground Coeff header:" << coefficient->header);
      if (coefficient->values.size() < 4){
        ROS_INFO_STREAM("coefficient extraction failed. Using preset coefficient.");
        coefficient = ground_coefficient;
        ground_indices = utilpcl::get_plane_inlier_indicies(cloud,coefficient,distance_threshold);
      }
    }
    // Remove the ground
    cloud = utilpcl::extract_pointcloud_by_indices(cloud, ground_indices, true);



    if (cloud->size() == 0){
      ROS_INFO_STREAM("cloud->size() = 0. Skipped");
      return;
    }


    // // Project the pointcloud on the floor
    // bool project_to_ground; ros::param::getCached("~project_to_ground",project_to_ground);
    // if (project_to_ground){
    //   cloud = utilpcl::project_pointcloud(cloud, coefficient);      
    // }


    // Dynmean Clustering

    int nRestarts = 10;

    // Fill clusterData for clustering
    std::vector<Eigen::Vector2f> clusterData;
    // ROS_INFO_STREAM("Size of cloud: " << cloud->size());
    for( pcl::PointCloud<pcl::PointXYZ>::iterator iter = cloud->begin(); iter != cloud->end(); ++iter)
    {
      Eigen::Vector2f v2d;
      v2d[0] = (*iter).x;
      v2d[1] = (*iter).y;
      clusterData.push_back(v2d);
      // clusterData.push_back((*iter).getVector3fMap());
    }

    ROS_INFO_STREAM("Size of clusterData: " << clusterData.size());

    // Performe dynmean clustering
    std::vector<Eigen::Vector2f> learnedParams;
    std::vector<int> learnedLabels;
    double tTaken, obj;
    dynm->cluster(clusterData, nRestarts, learnedLabels, learnedParams, obj, tTaken);
    ROS_INFO_STREAM("Size of learnedParams: " << learnedParams.size());
    ROS_INFO_STREAM("tTaken: " << tTaken << " obj: " << obj);

    // Convert dymean labels to cluster indicies
    std::vector<pcl::PointIndices> clusterVec = get_dynmean_cluster_indices(learnedLabels);
    for (int i = 0; i < clusterVec.size(); i++){
      ROS_INFO_STREAM("Cluster label: " << i << " has " << clusterVec[i].indices.size() << " points" );
    }

    // // Clustering
    // double cluster_dist_tolerance; ros::param::getCached("~cluster/dist_tolerance",cluster_dist_tolerance);
    // int min_cluster_size; ros::param::getCached("~cluster/min_size",min_cluster_size);
    // int max_cluster_size; ros::param::getCached("~cluster/max_size",max_cluster_size);
    // std::vector<pcl::PointIndices> clusterVec = utilpcl::get_euclidean_cluster_indices(cloud, cluster_dist_tolerance, min_cluster_size, max_cluster_size);

    // Generate markerArray for visualization
    visualization_msgs::MarkerArray markerArray;
    int j = 0;
    for (std::vector<pcl::PointIndices>::const_iterator indices = clusterVec.begin(); indices != clusterVec.end(); ++indices)
    {
      // Generate the markers for each cluster
      std::vector<visualization_msgs::Marker> markers = gen_markers_from_cluster(cloud, *indices, j, goal_frame);
      //Append the markers to the marker array
      markerArray.markers.insert(markerArray.markers.end(), markers.begin(), markers.end()); 
      j++;
    }
    pub_marker.publish(markerArray);
    ROS_INFO_STREAM("Size of markerArray: " << markerArray.markers.size() );

    // Convert and publish the processed point cloud
    sensor_msgs::PointCloud2 cloud_pub;
    pcl::toROSMsg(*cloud, cloud_pub);
    cloud_pub.header.frame_id = goal_frame; 
    cloud_pub.header.stamp = ros::Time::now();
    pub_cloud.publish(cloud_pub);
  }

  rgb get_color_by_id(int id)
  {
    // Return color by id. If no color is defined for id. Generate a random color

    // Fill colormap if empty
    if ( colormap.size() < (id + 1) ){
      rgb randomColor = { (float)(rand()%10)/10.0, (float)(rand()%10)/10.0, (float)(rand()%10)/10.0 };
      colormap.push_back(randomColor);
    }
    return colormap[id];
  }


  std::vector<pcl::PointIndices> get_dynmean_cluster_indices(std::vector<int> learnedLabels)
  {
    // Return a vector of pointindices corresponding given the learnedLabels from dynmean

    // Get the largetst label and initialize the vector of PointIndices 
    std::vector<int>::const_iterator it, it2;  
    it2 = max_element(learnedLabels.begin(), learnedLabels.end());
    std::vector<pcl::PointIndices> cluster_indices(*it2 + 1);

    // Iterate over the learnedLabels and 
    for (int i = 0; i < learnedLabels.size(); i++){
      int label = learnedLabels[i]; //label of point i
      cluster_indices[label].indices.push_back(i); //put point i into the cluster_indices according to the label
    }

    return cluster_indices;
  }


};


int main (int argc, char**argv)
{
  ros::init (argc, argv, "velodyne_cluster_node");
  VelodyneClusterClass velodyneClusterObj(argc, argv);

  // double lambda = 0.05;
  // double T_Q = 6.8;
  // double K_tau = 1.01;
  // double Q = lambda/T_Q;
  // double tau = (T_Q*(K_tau-1.0)+1.0)/(T_Q-1.0);
  // int nRestarts = 10;

  // DynMeans<Eigen::Vector2f> dynm(lambda, Q, tau);



  ros::spin();
  return 0;
}