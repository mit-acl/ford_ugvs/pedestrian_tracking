#include <ros/ros.h>

// For ROS interactions with PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

#include <cmath>

class VelodyneToLidar{
    public:
        ros::NodeHandle nh;
        ros::Publisher pub;
        ros::Subscriber sub;
        double height;
        double tolerance;

        VelodyneToLidar(int argc, char** argv){
            nh = ros::NodeHandle("~");
            pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 10);
            sub = nh.subscribe ("input", 5, &VelodyneToLidar::class_callback, this);
        }

        void class_callback(const sensor_msgs::PointCloud2& input){
            nh.getParam ("height", height);
            nh.getParam ("tolerance", tolerance);

            // convert message into pcl pointcloud rep
            pcl::PointCloud<pcl::PointXYZ>::Ptr vel_cloud(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::fromROSMsg (input, *vel_cloud);


            pcl::PointCloud<pcl::PointXYZ>::Ptr lidar_cloud(new pcl::PointCloud<pcl::PointXYZ>);
            
            //pcl::PointXYZ point;
            pcl::PointCloud<pcl::PointXYZ>::iterator it;

            for(it = vel_cloud->points.begin(); it < vel_cloud->points.end(); it++){
                if (std::abs(it->z - height) < tolerance){
                    lidar_cloud->push_back(*it);
                }
            }


            // convert output back into ROS msg
            sensor_msgs::PointCloud2 cloud_pub;
            pcl::toROSMsg(*lidar_cloud, cloud_pub);

            // put lidar cloud into same frame as input
            cloud_pub.header.frame_id = input.header.frame_id;
            cloud_pub.header.stamp = input.header.stamp;

            pub.publish(cloud_pub);
        }
};

int main (int argc, char**argv){
    ros::init (argc, argv, "velodyne_to_lidar");
    VelodyneToLidar v2l(argc, argv);
    ros::spin();
    return 0;
}
